import  sys
import os
import RegistryManagement
import ConnectionManagement
import wlstModule as wlst

def connectWLS():
    ConnectionManagement.connectWeblc(ConnectionManagement.getConnectDataWeblc(True))
    #ConnectionManagement.connectWeblc(False)

def printDataSourceStatus(dataSourceList):
    print "#### Retreiving DataSources status for domain: ", "\n\n"
    print "|%-30s|%-10s|%-25s|%-10s|%-10s|%-10s|" % ("NAME", "TYPE", "SERVER", "AC.AVG.CT", "AC.CUR.CT", "AC.HIG.CT")
    for ds in dataSourceList:
        print "|%-30s|%-10s|%-25s|%-10s|%-10s|%-10s|" % (ds['name'], ds['state'], ds['server'], ds['ac_avg_count'], ds['ac_cur_count'], ds['ac_hig_count'])
    print "\n\n"

def getDataSourceStatus():
    dataSourceList = []
    servers = wlst.domainRuntimeService.getServerRuntimes()
    if (len(servers) > 0):
        for server in servers:
            jdbcServiceRuntime = server.getJDBCServiceRuntime()
            dataSources = jdbcServiceRuntime.getJDBCDataSourceRuntimeMBeans()
            if (len(dataSources) > 0):
                for dataSource in dataSources:
                    ds = {}
                    ds['name'] = dataSource.getName()
                    ds['state'] = dataSource.getState()
                    ds['server'] = server.getName()
                    ds['ac_avg_count'] = dataSource.getActiveConnectionsAverageCount()
                    ds['ac_cur_count'] = dataSource.getActiveConnectionsCurrentCount()
                    ds['ac_hig_count'] = dataSource.getActiveConnectionsHighCount()
                    dataSourceList.append(ds)
    wlst.serverConfig()
    return dataSourceList

def main():
    try:
        connectWLS()
        dataSourceList = getDataSourceStatus()
        printDataSourceStatus(dataSourceList)
    except Exception, error:
        print error

if __name__ == "main":
    main()
