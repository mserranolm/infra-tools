import ConnectionManagement
import wlstModule as wlst
import datetime

#####must implement OnFragmentInteractionListener

HEADER = '\033[95m'
OK = '\033[92m'
WARN = '\033[93m'
FAIL = '\033[91m'
END ='\033[0m'

def connectWLS():
    ConnectionManagement.connectWeblc(ConnectionManagement.getConnectDataWeblc(True))
    #ConnectionManagement.connectWeblc(False)

def printServerStatus(serverList):
    print "#### Retreiving Servers status for domain: ", "\n\n"
    print "|%-25s|%-15s|%-45s|%-15s|%-15s|" % ("NAME", "STATE", "LST.ADDR", "LST.PRT", "HEALTH")
    for server in serverList:
        print "|%-25s|%-15s|%-45s|%-15s|%-15s|" % (
        server['name'], server['state'], server['listen_address'], server['listen_port'], server['health_state'])
    print "\n\n"

    print "#### Getting status for thread pool: ", "\n\n"
    print "|%-30s|%-14s|%-18s|%-18s|%-18s|" % ("NAME", "IDDLE.THREADS", "TOTAL.THREADS", "HOGG.THREADS", "THREADPOOL.STATE")
    for server in serverList:
        print "|%-30s|%-14s|%-18s|%-18s|%-18s|" % (
        server['name'], server['execute_thread_idle'], server['execute_thread_total'], server['hogging_thread'], server['thread_health_state'])
    print "\n\n"

    print "#### Status JVM: ", "\n\n"
    print "|%-30s|%-14s|%-15s|%-18s|%-20s|%-15s|%-12s|" % ("NAME", "HEAP.FREE.MB", "HEAP.FREE.%", "CURRENT.HEAP.MB", "JAVA.VENDOR", "JAVA.VERSION", "UPTIME.HRS")
    for server in serverList:
        print "|%-30s|%-14s|%-15s|%-18s|%-20s|%-15s|%-12s|" % (
        server['name'], server['heap_free'], server['heap_free_percent'], server['heap_size_current'], server['java_vendor'], server['java_version'], server['uptime'])
    print "\n\n"

def getServerStatus():
    serverList = []
    servers = wlst.cmo.getServers()
    wlst.domainRuntime()
    for server in servers:
        s = {}
        serverLookup = wlst.domainRuntimeService.lookupServerRuntime(server.getName())
        if serverLookup != None:
            s['name'] = serverLookup.getName()
            s['state'] = serverLookup.getState()
            s['listen_address'] = serverLookup.getListenAddress()
            s['listen_port'] = serverLookup.getListenPort()
            statusHealth = None
            if (serverLookup.getHealthState().getState() == wlst.weblogic.health.HealthState.HEALTH_OK):
                statusHealth = "OK"
            elif (serverLookup.getHealthState().getState() == wlst.weblogic.health.HealthState.HEALTH_WARN):
                statusHealth = "WARNING"
            elif (serverLookup.getHealthState().getState() == wlst.weblogic.health.HealthState.HEALTH_CRITICAL):
                statusHealth = "CRITICAL"
            elif (serverLookup.getHealthState().getState() == wlst.weblogic.health.HealthState.HEALTH_FAILED):
                statusHealth = "FAILED"
            elif (serverLookup.getHealthState().getState() == wlst.weblogic.health.HealthState.HEALTH_OVERLOADED):
                statusHealth = "OVERLOADED"
            s['health_state'] = statusHealth


            s['execute_thread_total'] = serverLookup.getThreadPoolRuntime().getExecuteThreadTotalCount()
            s['hogging_thread'] = serverLookup.getThreadPoolRuntime().getHoggingThreadCount()
            #s['completed_request'] = serverLookup.getThreadPoolRuntime().getCompletedRequestCount()
            s['execute_thread_idle'] = serverLookup.getThreadPoolRuntime().getExecuteThreadIdleCount()


            statusThreadHealth = None
            if (str(serverLookup.getThreadPoolRuntime().getHealthState()).find("HEALTH_OK")!= -1):
                statusThreadHealth = "OK"
            elif (str(serverLookup.getThreadPoolRuntime().getHealthState()).find("HEALTH_WARN") != -1):
                statusThreadHealth = "WARNING"
            elif (str(serverLookup.getThreadPoolRuntime().getHealthState()).find("HEALTH_CRITICAL") != -1):
                statusThreadHealth = "CRITICAL"
            elif (str(serverLookup.getThreadPoolRuntime().getHealthState()).find("HEALTH_FAILED") != -1):
                statusThreadHealth = "FAILED"
            elif (str(serverLookup.getThreadPoolRuntime().getHealthState()).find("HEALTH_OVERLOADED") != -1):
                statusThreadHealth = "OVERLOADED"
            s['thread_health_state'] = statusThreadHealth   

            s['heap_free'] = serverLookup.getJVMRuntime().getHeapFreeCurrent() /1024 /1024
            s['heap_free_percent'] = serverLookup.getJVMRuntime().getHeapFreePercent()
            s['heap_size_current'] = serverLookup.getJVMRuntime().getHeapSizeCurrent() /1024 /1024
            #s['heap_size_max'] = serverLookup.getJVMRuntime().getHeapSizeMax()   /1024 /1024
            s['java_vendor'] = serverLookup.getJVMRuntime().getJavaVendor() 
            s['java_version'] = serverLookup.getJVMRuntime().getJavaVersion() 

            s['uptime'] = round(serverLookup.getJVMRuntime().getUptime()* 0.00001 * 1.666 /60,2)



        else:
            wlst.cd('/ServerLifeCycleRuntimes/' + server.getName())
            s['name'] = server.getName()
            s['state'] = wlst.get('State')
            s['listen_address'] = None
            s['listen_port'] = None
            s['health_state'] = None
            s['execute_thread_total'] = None
            s['hogging_thread'] = None
            s['completed_request'] = None
            s['execute_thread_idle'] = None
            s['thread_health_state'] = None 
            s['heap_free'] = None
            s['heap_free_percent'] = None
            s['heap_size_current'] = None
            s['heap_size_max'] = None
            s['java_vendor'] = None
            s['java_version'] = None
            s['uptime'] = None

        serverList.append(s)
    wlst.serverConfig()
    return serverList


def main():
    try:
        ConnectionManagement.connectWeblc(ConnectionManagement.getConnectDataWeblc(True))
        serverList = getServerStatus()
        printServerStatus(serverList)
    except Exception, error:
        print error


if __name__ == "main":
    main()
