select S.SID, S.SQL_ID,
       S.Username,
       SubStr(S.Program,1,15) Program,
       SubStr(OsUser,1,12)  OsUser,w.seconds_in_wait seconds, to_char(logon_time, 'DD-MON HH24:MI') Login,
       SubStr(w.Event,1,30) Event,
       SubStr(Machine,1,25) Machine
From   V$SESSION S, V$SESSION_WAIT W, V$PROCESS P
Where  S.SID = W.SID(+)
And    S.PAddr(+) = P.Addr
And    Status = 'ACTIVE'
And    S.Username is not null
Order By 9,4,8