package com.novopayment.infratools.od;


public class Script {
    private String name;
    private String file;
    private String description;
    private String type;
    private String scope;
    public boolean command;



    public Script(){
        this.name = "";
        this.file = "";
        this.description = "";
        this.type = "";
        this.scope = "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFile() {
        return file;
    }

    public void setFile(String file) {
        this.file = file;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getScope() {
        return scope;
    }

    public void setScope(String scope) {
        this.scope = scope;
    }

    public boolean isCommand() {
        return command;
    }

    public void setCommand(boolean command) {
        this.command = command;
    }

    @Override
    public String toString() {
        StringBuffer stringBuffer = new StringBuffer();
        stringBuffer.append("NAME: ".concat(this.name));
        stringBuffer.append("DESCRIPTION: ".concat(this.description));
        stringBuffer.append("TYPE: ".concat(this.type));
        stringBuffer.append("SCOPE: ".concat(this.scope));
        return stringBuffer.toString();
    }

    public String getNameExecution() {
        return name.replaceAll("\\s","");
    }

}
