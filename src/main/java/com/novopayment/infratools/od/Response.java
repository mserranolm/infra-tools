package com.novopayment.infratools.od;

import java.io.Serializable;

public class Response implements Serializable {
    private String code;
    private String response;

    public Response(){
    }

    public Response(String msn){
        this.code="0";
        this.response=msn;
    }

    public Response(Exception e){
        this.code="-1";
        this.response=e.getMessage();
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getResponse() {
        return response;
    }

    public void setResponse(String response) {
        this.response = response;
    }
}
