package com.novopayment.infratools.od;

public class Constants {
    final public static String PLANTILLA = "Plantilla.html";
    final public static String PLANTILLAWLS12 = "templates/wlst/WLS12BASH";
    final public static String PLANTILLAWLSCONNECT = "templates/wlst/connectWLS";
    final public static String PLANTILLAREVIEW = "templates/wlst/ServerStatus.py";
    final public static String BDPW = "templates/bd/";
    final public static String PATHPDF = "/tmp";
    final public static String EXTPDF = ".pdf";

    //++++++++++ TIPOS DE COMPONENTES ++++++++++

    //SERVIDORES
    final public static String LINUX_SERVER = "LINUX_SERVER";
    final public static String SOLARIS_SERVER = "SOLARIS_SERVER";
    final public static String AIX_SERVER = "AIX_SERVER";
    final public static String WINDOWS_SERVER = "WINDOWS_SERVER";


    //MIDDLEWARE
    final public static String WEBLOGIC_DOMAIN = "WEBLOGIC_DOMAIN";
    final public static String WEBLOGIC_ADMIN_SERVER = "WEBLOGIC_ADMIN_SERVER";
    final public static String WEBLOGIC_MANAGED_SERVER = "WEBLOGIC_MANAGED_SERVER";
    final public static String WILDFLY_DOMAIN = "WILDFLY_DOMAIN";
    final public static String WILDFLY_STANDALONE_SERVER = "WILDFLY_STANDALONE_SERVER";
    final public static String WILDFLY_MASTER_SERVER = "WILDFLY_MASTER_SERVER";
    final public static String WILDFLY_SLAVE_SERVER = "WILDFLY_SLAVE_SERVER";

    //DATABASE
    final public static String ORACLE_INSTANCE = "ORACLE_INSTANCE";
    final public static String MYSQL_DATABASE = "MYSQL_DATABASE";
    final public static String SQLSERVER_DATABASE = "SQLSERVER_DATABASE";

    //++++++++++ PROPIEDADES ++++++++++

    //GENERALES
    final public static String PRODUCTION_IP = "PRODUCTION_IP";
    final public static String ADMIN_IP = "ADMIN_IP";
    final public static String VIGILANT_IP = "VIGILANT_IP";
    final public static String PRODUCTION_PORT = "PRODUCTION_PORT";
    final public static String ADMIN_PORT = "ADMIN_PORT";
    final public static String VIGILANT_PORT = "VIGILANT_PORT";
    final public static String VERSION = "VERSION";
    final public static String ORACLE_HOME = "ORACLE_HOME";


    //MIDDLEWARE
    final public static String DOMAIN_HOME = "DOMAIN_HOME";
    final public static String WLST_SCRIPT = "WLST_SCRIPT";


    //DATABASE
    final public static String SERVICE_NAME = "SERVICE_NAME";
    final public static String SID = "SID";


    //++++++++++ TIPOS DE SCRIPTS ++++++++++
    // SCRIPT DE SO
    final public static String SHELL_SCRIPT = "SHELL_SCRIPT"; // SO LINUX
    final public static String KSH_SCRIPT = "KSH_SCRIPT"; // SO UNIX
    final public static String PYTHON_SCRIPT = "PYTHON_SCRIPT"; // PYTHON SCRIPT
    final public static String BATCH_SCRIPT = "BATCH_SCRIPT";
    final public static String SCP_SCRIPT = "SCP_SCRIPT";

    // SCRIPT DE BD
    final public static String SQLSERVER_QUERY = "SQLSERVER_QUERY";
    final public static String SQLSERVER_PROCEDURE = "SQLSERVER_PROCEDURE";
    final public static String ORACLE_QUERY = "ORACLE_QUERY";
    final public static String ORACLE_PROCEDURE = "ORACLE_PROCEDURE";
    final public static String MYSQL_QUERY = "MYSQL_QUERY";
    final public static String MYSQL_PROCEDURE = "MYSQL_PROCEDURE";








}
