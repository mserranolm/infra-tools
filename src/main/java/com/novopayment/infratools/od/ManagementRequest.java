package com.novopayment.infratools.od;


import java.io.Serializable;
import java.util.List;


public class ManagementRequest implements Serializable {
    private List<Checkup> checkups;

    public List<Checkup> getCheckups() {
        return checkups;
    }

    public void setCheckups(List<Checkup> checkups) {
        this.checkups = checkups;
    }
}
