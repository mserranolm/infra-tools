package com.novopayment.infratools.od;

import java.io.Serializable;

public class RequestNotified implements Serializable {
    private String apiToken;
    private String chat_id;
    private String text;



    public String getApiToken() {
        return apiToken;
    }

    public void setApiToken(String apiToken) {
        this.apiToken = apiToken;
    }

    public String getChat_id() {
        return chat_id;
    }

    public void setChat_id(String chat_id) {
        this.chat_id = chat_id;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }
}
