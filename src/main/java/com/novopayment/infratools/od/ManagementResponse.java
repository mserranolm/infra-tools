package com.novopayment.infratools.od;


import java.io.Serializable;

public class ManagementResponse implements Serializable {
    private Response response;

    public Response getResponse() {
        return response;
    }

    public void setResponse(Response response) {
        this.response = response;
    }


}
