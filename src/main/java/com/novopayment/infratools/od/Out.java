package com.novopayment.infratools.od;


import java.io.File;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;

/**
 * Created by mserrano on 3/8/18.
 */
public class Out {
    private HashMap<String,byte[]> file;
    private String name;
    private File pdf;
    private List<String> recipients;
    private StringBuffer html;
    private File path;
    //DETALLES de envio de correo
    private String subject;
    private String messageBody;
    private String title;
    private String user;


    public Out() {
        this.file =null;
        this.name = "";
        this.path = null;
        this.pdf = null;
        this.recipients = null;
        this.html = null;
        this.subject = "Ejecución y evidencia de Infra Tools";
        Calendar cal = Calendar.getInstance();
        DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        this.messageBody = null;
        this.title = "";
        this.user = "";
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public HashMap<String, byte[]> getFile() {
        return file;
    }

    public void setFile(HashMap<String, byte[]> file) {
        this.file = file;
    }

    public File getPdf() {
        return pdf;
    }

    public void setPdf(File pdf) {
        this.pdf = pdf;
    }

    public List<String> getRecipients() {
        return recipients;
    }

    public void setRecipients(List<String> recipients) {
        this.recipients = recipients;
    }

    public StringBuffer getHtml() {
        return html;
    }

    public void setHtml(StringBuffer html) {
        this.html = html;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getMessageBody() {
        return messageBody;
    }

    public void setMessageBody(String messageBody) {
        this.messageBody = messageBody;
    }

    public File getPath() {
        return path;
    }

    public void setPath(File path) {
        this.path = path;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    @Override
    public String toString() {
        return "Out{" +
                "file=" + file +
                ", name='" + name + '\'' +
                ", pdf=" + pdf +
                ", recipients=" + recipients +
                ", html=" + html +
                ", path=" + path +
                ", subject='" + subject + '\'' +
                ", messageBody='" + messageBody + '\'' +
                ", title='" + title + '\'' +
                ", user='" + user + '\'' +
                '}';
    }
}
