package com.novopayment.infratools.services.so;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

import java.io.IOException;
import java.sql.SQLException;

public interface ISoServices {
    public String getListExecute(Update update) throws IOException, SQLException;
    public String execute(Update update) throws IOException, SQLException;
}
