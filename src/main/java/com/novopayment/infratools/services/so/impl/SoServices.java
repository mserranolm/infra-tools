package com.novopayment.infratools.services.so.impl;

import com.novopayment.infratools.core.impl.IntegratorBusiness;
import com.novopayment.infratools.core.impl.LoadProperties;
import com.novopayment.infratools.od.Checkup;
import com.novopayment.infratools.od.Response;
import com.novopayment.infratools.od.Script;
import com.novopayment.infratools.services.so.ISoServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.api.objects.Update;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Logger;

@Slf4j
@Service
public class SoServices implements ISoServices {
    IntegratorBusiness integratorBusiness;
    private LoadProperties loadProperties;

    @Autowired
    public SoServices(IntegratorBusiness integratorBusiness, LoadProperties loadProperties) {
        this.integratorBusiness=integratorBusiness;
        this.loadProperties=loadProperties;
    }

    public String getListExecute(Update update) throws IOException, SQLException {
        final String origen = "SoBusiness.getListExecute";
        long time;
        Response response;

        String message="";
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(loadProperties.getCommand(update.getMessage().getText().split(" ")[2]));

            message=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public String execute(Update update) throws IOException, SQLException {
        final String origen = "SoBusiness.execute";
        long time;
        List<Checkup> checkups;
        Checkup checkup;
        Response response;

        String mensaje = "";
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(loadProperties.getEntry(update.getMessage().getText().split(" ")[1]));
            checkup=new Checkup();
            checkup.setHost(response.getResponse().split("\\|")[0]);
            checkup.setPort(response.getResponse().split("\\|")[1]);
            checkup.setUsername(response.getResponse().split("\\|")[2]);
            checkup.setPassword(response.getResponse().split("\\|")[3]);
            checkup.setComponent(response.getResponse().split("\\|")[4]);
            checkup.setName("EXEC");
            checkup.setTitle("EXEC ".concat(loadProperties.getCommand(update.getMessage().getText().split(" ")[1])));

            if(update.getMessage().getText().split(" ").length==2) {
                List<String> recipients = Arrays.asList(loadProperties.getGeneral("mail").split("\\|"));
                checkup.setRecipients(recipients);
            }else
            {
                List<String> recipients = new ArrayList<>();
                recipients.add(update.getMessage().getText().split(" ")[2]);
            }

            List<Script> scripts=new ArrayList<>();
            Script script=new Script();
            script.setName(response.getResponse().split("\\|")[5]);
            script.setType(response.getResponse().split("\\|")[6]);
            script.setFile(loadProperties.getCommand(update.getMessage().getText().split(" ")[1]));
            script.command=true;
            scripts.add(script);
            checkup.setScript(scripts);
            checkups=new ArrayList<>();
            checkups.add(checkup);
            response=integratorBusiness.integratorCore(checkups);

            mensaje=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            mensaje=mensaje.concat("ERROR EN LA EJECUCIÓN ").concat(e.getMessage());
        }

        return mensaje;
    }

}
