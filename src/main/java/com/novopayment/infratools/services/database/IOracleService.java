package com.novopayment.infratools.services.database;

import com.novopayment.infratools.od.Checkup;
import com.novopayment.infratools.od.Out;
import com.novopayment.infratools.od.Script;
import org.telegram.telegrambots.api.objects.Update;

import java.io.IOException;
import java.sql.SQLException;

public interface IOracleService {

    public String execute(Update update) throws IOException, SQLException;
    public StringBuffer executeDBQuery(Script script) throws IOException, SQLException;
    public StringBuffer executeDBProcedure(Script script) throws IOException, SQLException;
    public String executeMonitorDb() throws IOException, SQLException;
}
