package com.novopayment.infratools.services.database.impl;


import com.novopayment.infratools.chatbots.INotificationChatBoot;
import com.novopayment.infratools.core.impl.ConnectionOracle;
import com.novopayment.infratools.core.impl.IntegratorBusiness;
import com.novopayment.infratools.core.impl.LoadProperties;
import com.novopayment.infratools.helper.DBTablePrinterHelper;
import com.novopayment.infratools.helper.InfraHelper;
import com.novopayment.infratools.od.*;
import com.novopayment.infratools.services.database.IOracleService;
import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.OracleTypes;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.api.objects.Update;
import java.io.IOException;
import java.sql.CallableStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.*;
import java.util.logging.Logger;

@Slf4j
@Service
public class OracleService implements IOracleService {
    private LoadProperties loadProperties;
    private ConnectionOracle connectionsOracle;
    private IntegratorBusiness integratorBusiness;
    private INotificationChatBoot notificationChatBoot;
    private InfraHelper infraHelper;

    Statement statement;
    ResultSet resultSet;
    CallableStatement callableStatement;

    @Autowired
    public OracleService(ConnectionOracle connectionsOracle,LoadProperties loadProperties,IntegratorBusiness integratorBusiness,INotificationChatBoot notificationChatBoot,InfraHelper infraHelper) {
        this.connectionsOracle = connectionsOracle;
        this.loadProperties=loadProperties;
        this.integratorBusiness=integratorBusiness;
        this.notificationChatBoot=notificationChatBoot;
        this.infraHelper=infraHelper;
    }

    public String execute(Update update) throws IOException, SQLException {
        String response = null;
        Response responseTo=null;
        long time;
        try {
            time = System.currentTimeMillis();

            String connection = loadProperties.getBd(update.getMessage().getText().split(" ")[2]);

            connectionsOracle.connect(connection.split("\\|")[0], connection.split("\\|")[1], connection.split("\\|")[2], connection.split("\\|")[3], connection.split("\\|")[4], connection.split("\\|")[5]);
            statement = connectionsOracle.getConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            Script script = new Script();
            ClassLoader classLoader = getClass().getClassLoader();
            script.setFile(Base64.getEncoder().encodeToString(IOUtils.toString(classLoader.getResourceAsStream(Constants.BDPW)).getBytes()));
            script.setType(connection.split("\\|")[6]);

            if (script.getType().equalsIgnoreCase("ORACLE_QUERY")) {
                response=String.valueOf(executeDBQuery(script));
            } else if (script.getType().equalsIgnoreCase("ORACLE_PROCEDURE")) {
                response=(String.valueOf(executeDBProcedure(script)));
            }

            List<Checkup> checkups=new ArrayList<>();
            Checkup checkup=new Checkup();
            checkup.setScript(new ArrayList<>());
            checkup.getScript().add(script);

            List<String> recipients = new ArrayList<>();
            recipients.addAll(Arrays.asList(loadProperties.getGeneral("mail").split(";")));
            String usermail = update.getMessage().getText().split(" ")[3];
            if (!usermail.equalsIgnoreCase("NA"))
                recipients.add(usermail);
            checkup.setRecipients(recipients);

            checkup.setComponent("ORACLE_INSTANCE");
            checkup.setName("PROCESS_WAIT");
            checkup.setUsername(response);
            checkup.setTitle("BD EXECUTION");
            checkups.add(checkup);

            responseTo=integratorBusiness.integratorCore(checkups);

            if (responseTo.getResponse()!=null)
                response="Prueba Infra Tool";


            notificationChatBoot.sendNotified(response);


            time = System.currentTimeMillis() - time;

        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }finally {
            try {
                time = System.currentTimeMillis();
                connectionsOracle.closeJdbcObjects( statement,  resultSet);
                connectionsOracle.closeJdbcObjects( callableStatement,  resultSet);
                time = System.currentTimeMillis() - time;
            } catch (Exception e) {
                log.error("Failed to Execute, The error is" + e.getMessage());
            }
        }

        return response;
    }


    public StringBuffer executeDBQuery(Script script) throws IOException, SQLException {
        StringBuffer out = null;
        long time;
        try {
            time = System.currentTimeMillis();
            String executeCommand = new String(Base64.getDecoder().decode(script.getFile()));
            resultSet = statement.executeQuery(executeCommand);
            out = DBTablePrinterHelper.printResultSet(resultSet);
            time = System.currentTimeMillis() - time;
        } catch (Exception e) {

            log.error("Failed to Execute, The error is" + e.getMessage());
        }finally {
            try {
                time = System.currentTimeMillis();
                connectionsOracle.closeJdbcObjects( statement,  resultSet);
                connectionsOracle.closeJdbcObjects( callableStatement,  resultSet);
                time = System.currentTimeMillis() - time;
            } catch (Exception e) {
                log.error("Failed to Execute, The error is" + e.getMessage());
            }
        }

        return out;
    }


    public StringBuffer executeDBProcedure(Script script) throws IOException, SQLException {
        StringBuffer out= null;;
        long time;
        try {
            time = System.currentTimeMillis();
            String executeCommand = new String(Base64.getDecoder().decode(script.getFile().getBytes()));
            callableStatement = connectionsOracle.getConnection().prepareCall(executeCommand);
            callableStatement.registerOutParameter(1, OracleTypes.CURSOR);
            callableStatement.execute();
            ResultSet resultSet = (ResultSet) callableStatement.getObject(1);
            out = DBTablePrinterHelper.printResultSet(resultSet);
            time = System.currentTimeMillis() - time;

        } catch (Exception e) {

            log.error("Failed to Execute, The error is + e.getMessage()");
        }finally {
            try {
                time = System.currentTimeMillis();
                connectionsOracle.closeJdbcObjects( statement,  resultSet);
                connectionsOracle.closeJdbcObjects( callableStatement,  resultSet);
                time = System.currentTimeMillis() - time;
            } catch (Exception e) {
                log.error("Failed to Execute, The error is" + e.getMessage());
            }
        }

        return out;
    }

    public String executeMonitorDb() throws IOException, SQLException {
        String out = "";
        long time;
        try {
            time = System.currentTimeMillis();

            String connection = loadProperties.getBd("bd_prod");

            connectionsOracle.connect(connection.split("\\|")[0], connection.split("\\|")[1], connection.split("\\|")[2], connection.split("\\|")[3], connection.split("\\|")[4], connection.split("\\|")[5]);
            statement = connectionsOracle.getConnection().createStatement(ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_UPDATABLE);

            Map<String, String> notified_query_db= loadProperties.getNotified_query_db();

            for(String exec: notified_query_db.get("notified").split("\\|")) {

                Script script = new Script();
                script.setFile(infraHelper.getContentFile(Constants.BDPW + notified_query_db.get(exec).split("\\|")[0]));
                script.setType(connection.split("\\|")[6]);
                resultSet = statement.executeQuery(script.getFile());


                while (resultSet.next()) {
                    if (resultSet.getInt(notified_query_db.get(exec).split("\\|")[1]) > Integer.parseInt(notified_query_db.get(exec).split("\\|")[2])) {
                        out = out + " ";
                        out = (notified_query_db.get(exec).split("\\|")[3]) + "\n";
                        for(int i=4;i<notified_query_db.get(exec).split("\\|").length;i++) {
                            out = out +" "+ notified_query_db.get(exec).split("\\|")[i] + ": "+resultSet.getString(i-3) + "\n";
                            log.info(out);
                        }
                        notificationChatBoot.sendNotifiedJson(out);

                    }
                    log.info("Segundos " + resultSet.getInt(notified_query_db.get(exec).split("\\|")[1]));
                }
            }



            time = System.currentTimeMillis() - time;
        } catch (Exception e) {

            log.error("Failed to Execute, The error is" + e.getMessage());
        }finally {
            try {
                time = System.currentTimeMillis();
                connectionsOracle.closeJdbcObjects( statement,  resultSet);
                connectionsOracle.closeJdbcObjects( callableStatement,  resultSet);
                time = System.currentTimeMillis() - time;
            } catch (Exception e) {
                log.error("Failed to Execute, The error is" + e.getMessage());
            }
        }

        return out;
    }

    public ConnectionOracle getConnectionsOracle() {
        return connectionsOracle;
    }

    public void setConnectionsOracle(ConnectionOracle connectionsOracle) {
        this.connectionsOracle = connectionsOracle;
    }
}
