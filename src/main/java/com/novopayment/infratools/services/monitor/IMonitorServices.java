package com.novopayment.infratools.services.monitor;

import org.telegram.telegrambots.api.objects.Update;

import java.io.IOException;
import java.sql.SQLException;

public interface IMonitorServices {

    public String liveUrl() throws IOException;

}
