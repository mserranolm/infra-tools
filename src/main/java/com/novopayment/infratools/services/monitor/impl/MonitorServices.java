package com.novopayment.infratools.services.monitor.impl;

import com.novopayment.infratools.chatbots.INotificationChatBoot;
import com.novopayment.infratools.core.impl.ConnectionSSH;
import com.novopayment.infratools.core.impl.IntegratorBusiness;
import com.novopayment.infratools.core.impl.LoadProperties;
import com.novopayment.infratools.core.impl.SshBusiness;
import com.novopayment.infratools.od.*;
import com.novopayment.infratools.services.monitor.IMonitorServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

import java.io.IOException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Logger;

@Slf4j
@Service
public class MonitorServices implements IMonitorServices {
    private LoadProperties loadProperties;
    private INotificationChatBoot notificationChatBoot;


    @Autowired
    public MonitorServices(LoadProperties loadProperties,INotificationChatBoot notificationChatBoot) {
        this.notificationChatBoot=notificationChatBoot;
        this.loadProperties=loadProperties;
    }


    public String liveUrl() throws IOException {
        final String origen = "MonitorServices.liveUrl";
        long time;

        String result = "";
        String url = "";
        int code = 200;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            String [] hostList=loadProperties.getHealthcheck("url");

            for (int i = 0; i < hostList.length; i++) {
                url = hostList[i];
                URL siteURL = new URL(url);
                HttpURLConnection connection = (HttpURLConnection) siteURL.openConnection();
                connection.setRequestMethod("GET");
                connection.setConnectTimeout(3000);
                connection.connect();
                code = connection.getResponseCode();

                if (code == 200) {
                    result = "Code: " + code + " URL "+url+" success";
                    ;
                } else {
                    result = "Code: " + code + " URL "+url+" failed";
                    notificationChatBoot.sendNotified(result);
                }

                log.info("Respuesta: "+result);
            }

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);

        } catch (Exception e) {
            result = "Code: " + code + " URL "+url+ e.getMessage();
        }
        return result;
    }

}
