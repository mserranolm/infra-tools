package com.novopayment.infratools.services.middleware.impl;

import com.jcraft.jsch.SftpException;
import com.novopayment.infratools.core.impl.*;
import com.novopayment.infratools.od.*;
import com.novopayment.infratools.services.middleware.IWeblogicServices;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Update;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import java.util.*;

@Slf4j
@Service
public class WeblogicServices implements IWeblogicServices {
    IntegratorBusiness integratorBusiness;
    private LoadProperties loadProperties;
    SshBusiness sshBusiness;
    ByteArrayOutputStream bous;
    ReportBusiness reportBusiness;


    @Autowired
    public WeblogicServices(IntegratorBusiness integratorBusiness, LoadProperties loadProperties,ConnectionSSH connectionSSH,SshBusiness sshBusiness,ReportBusiness reportBusiness) {
        this.integratorBusiness=integratorBusiness;
        this.loadProperties=loadProperties;
        this.sshBusiness=sshBusiness;
        this.reportBusiness=reportBusiness;
    }


    public String deployWeblogic(Update update, String parametros) throws IOException {
        final String origen = "WeblogicServices.deployWeblogic";
        long time;
        String mensaje = "";
        StringBuffer script;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            String connection = loadProperties.getDeploy(parametros.split(" ")[1]);
            //HACER DESPLIEGUE
            response=doDeploy(update,connection,parametros);

            if (response.getResponse().contains("completed")) {
                //MOVER WAR DE SERVER
                doMoveWar(connection, parametros, response.getResponse().split(" ")[55].replace("-cluster\n", ""));
            }

            mensaje=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            mensaje=mensaje.concat("ERROR EN LA EJECUCIÓN ").concat(e.getMessage());
        }

        return mensaje;
    }


    public String undeployWeblogic(Update update,String parametros) throws IOException {
        final String origen = "WeblogicServices.undeployWeblogic";
        long time;
        String mensaje = "";
        StringBuffer script;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            String connection = loadProperties.getDeploy(parametros.split(" ")[1]);
            //HACER DESPLIEGUE
            response=doUndeploy(update,connection,parametros);
            mensaje=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            mensaje=mensaje.concat("ERROR EN LA EJECUCIÓN ").concat(e.getMessage());
        }

        return mensaje;
    }


    public String reDeployWeblogic(Update update,String parametros) throws IOException {
        final String origen = "WeblogicServices.reDeployWeblogic";
        long time;
        String mensaje = "";
        StringBuffer script;
        Response response = null;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            String connection = loadProperties.getDeploy(parametros.split(" ")[1]);

            String war=doFindWar(update,connection,parametros);
            List<String> listWar= Arrays.asList(war.split("\\n"));

            response=new Response();
            if(listWar.size()>1) {
                //HACER DESPLIEGUE
                response = doRedeploy(update, connection, parametros);

                if (response.getResponse().contains("completed")) {
                    //METODO DE BACKUP
                    //doBackupWar(connection, parametros, response.getResponse().split(" ")[55].replace("-cluster\n", ""));
                    //MOVER WAR DE SERVER
                    doMoveWar(connection, parametros, response.getResponse().split(" ")[55].replace("-cluster\n", ""));
                }

            }else{
                response.setResponse("El WAR no existe en el Weblogic para realizar su despliegue");
            }

            mensaje=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            mensaje=mensaje.concat("ERROR EN LA EJECUCIÓN ").concat(e.getMessage());
        }

        return mensaje;
    }

    public String executePy(Update update) throws IOException {
        final String origen = "WeblogicServices.executePy";
        long time;
        String command="";
        String py="";

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            Checkup checkup=new Checkup();
            //CARGAR CONEXION
            String connection = loadProperties.getDeploy(update.getMessage().getText().split(" ")[1]);
            checkup.setHost(connection.split("\\|")[5]);
            checkup.setPort(connection.split("\\|")[6]);
            checkup.setUsername(connection.split("\\|")[7]);
            checkup.setPassword(connection.split("\\|")[8]);
            checkup.setName(update.getMessage().getText().split(" ")[1].concat("Review"));
            checkup.setComponent("WEBLOGIC_DOMAIN");
            checkup.setName("REVIEW-STATUS-SERVER");
            checkup.setTitle("REVIEW ".concat(loadProperties.getDeploy(update.getMessage().getText().split(" ")[1]).split("\\|")[3]));

            //SE LIMPIA LOS ESPACIO EN BLANCO PARA EN EL NOMBRE DE LOS DIRECTORIOS QUE TENDRAS LOS SCRIPT
            String path=("/tmp/".concat(checkup.getName()+ File.separator).replaceAll("\\s+",""));

            List<Script> scriptPys = new ArrayList<>();
            Script scriptPy=new Script();
            scriptPy.setName("mkdir");
            scriptPy.setType("BATCH_SCRIPT");
            scriptPy.setFile("mkdir -p ".concat(path));
            scriptPy.command=true;
            scriptPys.add(scriptPy);
            checkup.setScript(scriptPys);

            //SE CREA EL DIRECTORIO
            Out out=new Out();
            out=sshBusiness.execute(checkup);

            ClassLoader classLoader = getClass().getClassLoader();

            //GETPY
            scriptPys = new ArrayList<>();
            scriptPy=new Script();
            scriptPy.setName("REVIEW-STATUS-SERVER".concat(".py"));
            scriptPy.setType("BATCH_SCRIPT");
            scriptPy.setFile(Base64.getEncoder().encodeToString(IOUtils.toString(classLoader.getResourceAsStream(Constants.PLANTILLAREVIEW)).getBytes()));
            scriptPy.command=true;
            scriptPys.add(scriptPy);
            checkup.setScript(scriptPys);

            //SE COPIA EL ARCHIVO
            sshBusiness.sendFileSCP(checkup, path);

            //SE CARGA PLANTILLA DE EJECUCION
            command= IOUtils.toString(classLoader.getResourceAsStream(Constants.PLANTILLAWLS12));

            //SE DEFINE EL DIRECTORIO A CREAR
            command=command.replaceAll("@PATH@",path);

            //SE DEFINE EL ORACLE HOME
            command=command.replaceAll("@ORACLE_HOME@",loadProperties.getDeploy(update.getMessage().getText().split(" ")[1]).split("\\|")[4]);

            //SE DEFINE EL NOMBRE DEL SCRIPT A EJECUTAR
            command=command.replaceAll("@SCRIPT@",checkup.getName().concat(".py"));

            //SE CARGA PLANTILLA DE EJECUCION
            py=IOUtils.toString(classLoader.getResourceAsStream(Constants.PLANTILLAWLSCONNECT));


            //SE DEFINE HOST
            py=py.replaceAll("@HOST@",connection.split("\\|")[5]);

            //SE DEFINE PORT
            py=py.replaceAll("@PORT@",connection.split("\\|")[6]);

            //SE DEFINE USUARIO
            py=py.replaceAll("@USERNAME@",connection.split("\\|")[7]);

            //SE DEFINE PASSWORD
            py=py.replaceAll("@PASSWORD@",connection.split("\\|")[8]);

            //SE INTEGRA EL PY LA CONEXION CON EL PY A CORRER
            py=py+"\n\n".concat(command);

            scriptPys = new ArrayList<>();
            scriptPy=new Script();
            scriptPy.setName("SCP-EXEC-PY");
            scriptPy.setType("BATCH_SCRIPT");
            scriptPy.setFile(Base64.getEncoder().encodeToString(py.getBytes()));
            scriptPy.command=false;
            scriptPys.add(scriptPy);
            checkup.setScript(scriptPys);

            //SE EXEC WLST
            out=new Out();
            out=sshBusiness.execute(checkup);


            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return py;
    }


    public String getListDeploy(Update update) throws IOException {
        final String origen = "WeblogicServices.getListDeploy";
        long time;
        String mensaje = "";
        StringBuffer script;
        Response response = null;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            CallbackQuery callbackquery = update.getCallbackQuery();
            String[] data = callbackquery.getData().split(":");

            String connection = loadProperties.getDeploy(data[0]);

            Script scriptExec=new Script();
            scriptExec=new Script();
            List<Script> scripts=new ArrayList<>();
            String scriptListar = new String();
            scriptListar=scriptListar.concat("cd ").concat(loadProperties.getGeneral("repositorywar")).concat(" && ").concat(loadProperties.getGeneral("listarwar"));
            scriptExec=new Script();
            scriptExec.setName("LISTAR WAR");
            scriptExec.setType("BATCH_SCRIPT");
            scriptExec.setFile(scriptListar);
            scriptExec.command=true;
            scripts.add(scriptExec);

            Checkup checkup=new Checkup();
            List<Checkup> checkups=new ArrayList<>();
            checkup.setHost(connection.split("\\|")[0]);
            checkup.setPort(connection.split("\\|")[1]);
            checkup.setUsername(connection.split("\\|")[2]);
            checkup.setPassword(connection.split("\\|")[3]);
            checkup.setComponent("WEBLOGIC_DOMAIN");
            checkup.setName("LISTAR WAR");
            checkup.setTitle("LISTAR WAR");
            checkup.setScript(scripts);
            checkups.add(checkup);

            Out o=sshBusiness.execute(checkups.get(0));
            response=new Response();
            response.setResponse(o.getMessageBody());
            mensaje=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            mensaje=mensaje.concat("ERROR EN LA EJECUCIÓN ").concat(e.getMessage());
        }

        return mensaje;
    }

    public String getListDeployForManaged(Update update,String managed) throws IOException {
        final String origen = "WeblogicServices.getListDeployForManaged";
        long time;
        String mensaje = "";
        StringBuffer script;
        Response response = null;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            CallbackQuery callbackquery = update.getCallbackQuery();
            String[] data = callbackquery.getData().split(":");

            String connection = loadProperties.getDeploy(data[0]);

            Script scriptExec=new Script();
            scriptExec=new Script();
            List<Script> scripts=new ArrayList<>();
            String scriptListar = new String();
            scriptListar=scriptListar.concat("cd ").concat(loadProperties.getGeneral("domainpath").concat("servers/").concat(managed.concat("-1")).concat("/stage")).concat(" && ").concat(loadProperties.getGeneral("listarwar"));
            scriptExec=new Script();
            scriptExec.setName("LISTAR WAR");
            scriptExec.setType("BATCH_SCRIPT");
            scriptExec.setFile(scriptListar);
            scriptExec.command=true;
            scripts.add(scriptExec);

            Checkup checkup=new Checkup();
            List<Checkup> checkups=new ArrayList<>();
            checkup.setHost(connection.split("\\|")[0]);
            checkup.setPort(connection.split("\\|")[1]);
            checkup.setUsername(connection.split("\\|")[2]);
            checkup.setPassword(connection.split("\\|")[3]);
            checkup.setComponent("WEBLOGIC_DOMAIN");
            checkup.setName("LISTAR WAR");
            checkup.setTitle("LISTAR WAR");
            checkup.setScript(scripts);
            checkups.add(checkup);

            Out o=sshBusiness.execute(checkups.get(0));
            response=new Response();
            response.setResponse(o.getMessageBody());
            mensaje=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            mensaje=mensaje.concat("ERROR EN LA EJECUCIÓN ").concat(e.getMessage());
        }

        return mensaje;
    }

    public void doBackupWar(String connection,String parametros,String managed){


        try {
            //SE EJECUTA BACKUP DE WAR
            Checkup checkup=new Checkup();
            List<Checkup> checkups;
            checkups=new ArrayList<>();

            //HACER BACKUP DE WAR
            Script scriptExec=new Script();
            List<Script> scripts=new ArrayList<>();
            String scriptBck = new String();
            SimpleDateFormat formatDate = new SimpleDateFormat("dd-MM-YYYY");
            SimpleDateFormat formatTime = new SimpleDateFormat("HH:mm:ss.SSS");
            String dateString = formatDate.format( new Date() );
            String timeString = formatTime.format( new Date() );
            scriptBck=scriptBck.concat("mv ").concat(loadProperties.getGeneral("target_war").concat(managed).concat("/").concat(parametros.split(" ")[3])).concat(" ").concat(loadProperties.getGeneral("target_war").concat(managed.concat("/").concat(parametros.split(" ")[3]))).concat(".bck-").concat(dateString).concat("-").concat(timeString);
            scriptExec=new Script();
            scriptExec.setName("DEPLOY BACKUP");
            scriptExec.setType("BATCH_SCRIPT");
            scriptExec.setFile(scriptBck);
            scriptExec.command=true;
            scripts.add(scriptExec);

            checkup.setHost(connection.split("\\|")[0]);
            checkup.setPort(connection.split("\\|")[1]);
            checkup.setUsername(connection.split("\\|")[2]);
            checkup.setPassword(connection.split("\\|")[3]);
            checkup.setComponent("WEBLOGIC_DOMAIN");
            checkup.setName("BACKUP WAR");
            checkup.setTitle("BACKUP ".concat(loadProperties.getDeploy(parametros.split(" ")[1]).split("\\|")[3]));
            checkup.setScript(scripts);
            checkups.add(checkup);

            Out o=sshBusiness.execute(checkups.get(0));

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void doMoveWar(String connection,String parametros,String managed){
        try {

            //SE EJECUTA BACKUP DE WAR
            Checkup checkup=new Checkup();
            List<Checkup> checkups;
            checkups=new ArrayList<>();

            //MOVER WAR
            Script scriptExec=new Script();
            List<Script> scripts=new ArrayList<>();
            String scriptBck = new String();
            if(!managed.equalsIgnoreCase("completed"))
                scriptBck=scriptBck.concat("mv ").concat(loadProperties.getGeneral("repositorywar").concat(parametros.split(" ")[3])).concat(" ").concat(loadProperties.getGeneral("target_war").concat(managed.concat("/").concat(parametros.split(" ")[3])));
            else
                scriptBck=scriptBck.concat("mv ").concat(loadProperties.getGeneral("repositorywar").concat(parametros.split(" ")[3])).concat(" ").concat(loadProperties.getGeneral("target_war").concat(parametros.split(" ")[2]).concat("/").concat(parametros.split(" ")[3]));
            scriptExec.setName("DEPLOY BACKUP");
            scriptExec.setType("BATCH_SCRIPT");
            scriptExec.setFile(scriptBck);
            scriptExec.command=true;
            scripts.add(scriptExec);

            checkup.setHost(connection.split("\\|")[0]);
            checkup.setPort(connection.split("\\|")[1]);
            checkup.setUsername(connection.split("\\|")[2]);
            checkup.setPassword(connection.split("\\|")[3]);
            checkup.setComponent("WEBLOGIC_DOMAIN");
            checkup.setName("BACKUP WAR");
            checkup.setTitle("BACKUP ".concat(loadProperties.getDeploy(parametros.split(" ")[1]).split("\\|")[3]));
            checkup.setScript(scripts);
            checkups.add(checkup);

            Out o=sshBusiness.execute(checkups.get(0));


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<Checkup> coreWLS(Update update,String connection,String commandDeploy, String name,String parametros){
        List<Checkup> checkups=new ArrayList<>();

        try {

            //PREPARAR SCRIPT DE DESPLIEGUE

            Checkup checkup=new Checkup();
            Script scriptExec=new Script();
            List<Script> scripts=new ArrayList<>();

            StringBuffer script=new StringBuffer();
            script.append(". ".concat(loadProperties.getDeploy(parametros.split(" ")[1]).split("\\|")[7]).concat(loadProperties.getGeneral("wls12_path_bin").concat(loadProperties.getGeneral("setWLS"))));


            //SE SETEA EL HOST
            commandDeploy=commandDeploy.replace("@host@",connection.split("\\|")[0]);
            //SE SETEA EL PORT
            commandDeploy=commandDeploy.replace("@port@",connection.split("\\|")[4]);
            //SE SETEA EL USERNAME
            commandDeploy=commandDeploy.replace("@user@ ",connection.split("\\|")[5]);
            //SE SETEA EL PASSWORD
            commandDeploy=commandDeploy.replace("@pass@",connection.split("\\|")[6]);
            //SE SETEA EL WAR
            if(parametros.split(" ")[3].contains(".war"))
                commandDeploy=commandDeploy.replace("@war@",parametros.split(" ")[3]);
            else
                commandDeploy=commandDeploy.replace("@war@",parametros.split(" ")[3].concat(".war"));
            //SE SETEA EL PATH WAR
            commandDeploy=commandDeploy.replace("@path@",loadProperties.getGeneral("repositorywar").concat(parametros.split(" ")[3]));
            //SE SETEA EL TARGET
            if(!parametros.split(" ")[2].contains("Redesplegar"))
                commandDeploy=commandDeploy.replace("@target@",parametros.split(" ")[2].concat("-cluster"));
            script.append(System.lineSeparator());
            script.append(commandDeploy);

            checkup.setHost(connection.split("\\|")[0]);
            checkup.setPort(connection.split("\\|")[1]);
            checkup.setUsername(connection.split("\\|")[2]);
            checkup.setPassword(connection.split("\\|")[3]);
            checkup.setComponent("WEBLOGIC_DOMAIN");
            checkup.setUserTelegram(update.getCallbackQuery().getFrom().getFirstName().concat(" ").concat(update.getCallbackQuery().getFrom().getLastName()));
            checkup.setName(name.concat("-").concat(parametros.split(" ")[3]));
            checkup.setTitle("(".concat(name).concat(") Ambiente: ").concat(loadProperties.getAmbientes(parametros.split(" ")[1])));

            List<String> recipients = new ArrayList<>();
            recipients.addAll(Arrays.asList(loadProperties.getGeneral("mail").split(";")));
            checkup.setRecipients(recipients);

            //SE EJECUTA DESPLIEGUE
            scriptExec.setName(name.concat("-").concat(parametros.split(" ")[3]));
            scriptExec.setType("BATCH_SCRIPT");
            scriptExec.setFile(String.valueOf(script));
            scriptExec.command=true;
            scripts.add(scriptExec);
            checkup.setScript(scripts);
            checkups.add(checkup);

        } catch (Exception e) {
            e.printStackTrace();
        }

        return checkups;
    }

    public Response doDeploy(Update update, String connection,String parametros){
        Response response = null;
        try {

            List<Checkup> checkups=coreWLS(update,connection,loadProperties.getGeneral("deploy_command"),"DEPLOY",parametros);
            response = new Response();
            response=integratorBusiness.integratorCore(checkups);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return response;
    }

    public Response doUndeploy(Update update, String connection,String parametros){
        Response response = null;
        try {

            List<Checkup> checkups=coreWLS(update,connection,loadProperties.getGeneral("undeploy_command"),"UNDEPLOY",parametros);
            response = new Response();
            response=integratorBusiness.integratorCore(checkups);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return response;
    }

    public Response doRedeploy(Update update, String connection,String parametros){
        Response response = null;
        try {

            List<Checkup> checkups=coreWLS(update,connection,loadProperties.getGeneral("redeploy_command"),"REDEPLOY",parametros);
            response = new Response();
            response=integratorBusiness.integratorCore(checkups);

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return response;
    }

    public String getListProperties(Update update,String managed) throws IOException {
        final String origen = "WeblogicServices.getListProperties";
        long time;
        String mensaje = "";
        StringBuffer script;
        Response response = null;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            CallbackQuery callbackquery = update.getCallbackQuery();
            String[] data = callbackquery.getData().split(":");

            String connection = loadProperties.getDeploy(data[0]);

            Script scriptExec=new Script();
            scriptExec=new Script();
            List<Script> scripts=new ArrayList<>();
            String scriptListar = new String();
            scriptListar=scriptListar.concat("cd ").concat(loadProperties.getGeneral("repositoryproperties")).concat(" && ").concat(loadProperties.getGeneral("listarwar"));
            scriptExec=new Script();
            scriptExec.setName("LISTAR WAR");
            scriptExec.setType("BATCH_SCRIPT");
            scriptExec.setFile(scriptListar);
            scriptExec.command=true;
            scripts.add(scriptExec);

            Checkup checkup=new Checkup();
            List<Checkup> checkups=new ArrayList<>();
            checkup.setHost(connection.split("\\|")[0]);
            checkup.setPort(connection.split("\\|")[1]);
            checkup.setUsername(connection.split("\\|")[2]);
            checkup.setPassword(connection.split("\\|")[3]);
            checkup.setComponent("WEBLOGIC_DOMAIN");
            checkup.setName("LISTAR WAR");
            checkup.setTitle("LISTAR WAR");
            checkup.setScript(scripts);
            checkups.add(checkup);

            Out o=sshBusiness.execute(checkups.get(0));
            response=new Response();
            response.setResponse(o.getMessageBody());
            mensaje=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            mensaje=mensaje.concat("ERROR EN LA EJECUCIÓN ").concat(e.getMessage());
        }

        return mensaje;
    }

    public String getListPropertiesPath(Update update,String properties) throws IOException {
        final String origen = "WeblogicServices.getListPropertiesPath";
        long time;
        String mensaje = "";
        StringBuffer script;
        Response response = null;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            CallbackQuery callbackquery = update.getCallbackQuery();
            String[] data = callbackquery.getData().split(":");

            String connection = loadProperties.getDeploy(data[0]);

            Script scriptExec=new Script();
            scriptExec=new Script();
            List<Script> scripts=new ArrayList<>();
            String scriptListar = new String();
            scriptListar=scriptListar.concat(loadProperties.getGeneral("clistproperties").concat(" '").concat(data[2]).concat("'"));
            scriptExec=new Script();
            scriptExec.setName("LISTAR WAR");
            scriptExec.setType("BATCH_SCRIPT");
            scriptExec.setFile(scriptListar);
            scriptExec.command=true;
            scripts.add(scriptExec);

            Checkup checkup=new Checkup();
            List<Checkup> checkups=new ArrayList<>();
            checkup.setHost(connection.split("\\|")[0]);
            checkup.setPort(connection.split("\\|")[1]);
            checkup.setUsername(connection.split("\\|")[2]);
            checkup.setPassword(connection.split("\\|")[3]);
            checkup.setComponent("WEBLOGIC_DOMAIN");
            checkup.setName("LISTAR WAR");
            checkup.setTitle("LISTAR WAR");
            checkup.setScript(scripts);
            checkups.add(checkup);

            Out o=sshBusiness.execute(checkups.get(0));
            response=new Response();
            response.setResponse(o.getMessageBody());
            mensaje=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            mensaje=mensaje.concat("ERROR EN LA EJECUCIÓN ").concat(e.getMessage());
        }

        return mensaje;
    }

    public String doMoveProperties(Update update,String parametros,String path,String propiedad) throws IOException {
        final String origen = "WeblogicServices.doMoveProperties";
        long time;
        String mensaje = "";
        StringBuffer script;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            String connection = loadProperties.getDeploy(parametros);

            //SE EJECUTA BACKUP DE WAR
            Checkup checkup=new Checkup();
            List<Checkup> checkups;
            checkups=new ArrayList<>();

            List<String> destino= Arrays.asList(getListPropertiesPath(update, path).split("\\n"));

            //MOVER WAR
            Script scriptExec=new Script();
            List<Script> scripts=new ArrayList<>();
            String scriptBck = new String();
            scriptBck=scriptBck.concat("mv ").concat(loadProperties.getGeneral("repositoryproperties").concat("/").concat(propiedad)).concat(" ").concat(destino.get(Integer.parseInt(path)-1));
            scriptExec.setName("PROPERTIES");
            scriptExec.setType("BATCH_SCRIPT");
            scriptExec.setFile(scriptBck);
            scriptExec.command=true;
            scripts.add(scriptExec);

            checkup.setHost(connection.split("\\|")[0]);
            checkup.setPort(connection.split("\\|")[1]);
            checkup.setUsername(connection.split("\\|")[2]);
            checkup.setPassword(connection.split("\\|")[3]);
            checkup.setComponent("WEBLOGIC_DOMAIN");
            checkup.setName("PROPERTIES");
            checkup.setTitle("PROPERTIES ");
            checkup.setScript(scripts);
            checkups.add(checkup);

            Out o=sshBusiness.execute(checkups.get(0));

            String ambiente="";
            switch (connection.split("\\|")[0]){
                case "weblogic_dev" :
                    ambiente="Dev";
                break;
                case "weblogic_tes" :
                    ambiente="Test";
                    break;
                case "weblogic_uat" :
                    ambiente="UAT";
                    break;
            }

            Out out=new Out();
            out.setUser(update.getCallbackQuery().getFrom().getFirstName().concat(" ").concat(update.getCallbackQuery().getFrom().getLastName()));
            out.setName("pase".concat(ambiente));
            out.setRecipients(Arrays.asList(loadProperties.getGeneral("mail").split(";")));
            out.setTitle("Solicitud pase a ".concat(ambiente));
            out.setMessageBody(loadProperties.getGeneral("message_body"));
            out.setMessageBody(out.getMessageBody().concat(" ").concat("Solicitud de pase a ").concat(ambiente));
            HashMap<String,byte[]> evidencia= new HashMap<>();
            evidencia.put("Pase".concat(ambiente),mensaje.concat("Pase ").concat(ambiente).concat(" del properties ").concat(propiedad).concat(" enviado").concat(System.lineSeparator()).getBytes());
            out.setFile(evidencia);

            response=new Response();
            response.setResponse(mensaje.concat("Pase ").concat(ambiente).concat(" del properties ").concat(propiedad).concat(" enviado"));
            mensaje=response.getResponse();

            reportBusiness.ReportCore(out);

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            mensaje=mensaje.concat("ERROR EN LA EJECUCIÓN ").concat(e.getMessage());
        }

        return mensaje;
    }

    public String doFindWar(Update update,String connection,String war) throws IOException {
        final String origen = "WeblogicServices.doFindWar";
        long time;
        String mensaje = "";
        StringBuffer script;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            //SE EJECUTA BACKUP DE WAR
            Checkup checkup=new Checkup();
            List<Checkup> checkups;
            checkups=new ArrayList<>();

            //MOVER WAR
            Script scriptExec=new Script();
            List<Script> scripts=new ArrayList<>();
            String scriptBck = new String();
            scriptBck=scriptBck.concat("cd ".concat(loadProperties.getGeneral("wars").concat(" &&")).concat(loadProperties.getGeneral("findwar").concat(" ").concat(war.split(" ")[3])));
            scriptExec.setName("FIND");
            scriptExec.setType("BATCH_SCRIPT");
            scriptExec.setFile(scriptBck);
            scriptExec.command=true;
            scripts.add(scriptExec);

            checkup.setHost(connection.split("\\|")[0]);
            checkup.setPort(connection.split("\\|")[1]);
            checkup.setUsername(connection.split("\\|")[2]);
            checkup.setPassword(connection.split("\\|")[3]);
            checkup.setComponent("WEBLOGIC_DOMAIN");
            checkup.setName("FIND");
            checkup.setTitle("FIND ");
            checkup.setScript(scripts);
            checkups.add(checkup);

            Out o=sshBusiness.execute(checkups.get(0));

            response=new Response();
            response.setResponse(o.getMessageBody());
            mensaje=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            mensaje=mensaje.concat("ERROR EN LA EJECUCIÓN ").concat(e.getMessage());
        }

        return mensaje;
    }


    public String deployWeblogicPre(Update update,String parametros) throws IOException {
        final String origen = "WeblogicServices.deployWeblogicPre";
        long time;

        String mensaje="";
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();
            Checkup checkup = new Checkup();
            String connection;
            String ambiente = "";

            if(parametros.split(" ")[1].equalsIgnoreCase("weblogic_tes")) {
                connection = loadProperties.getDeploy("weblogic_dev");
                checkup.setHost(connection.split("\\|")[0]);
                checkup.setPort(connection.split("\\|")[1]);
                checkup.setUsername(connection.split("\\|")[2]);
                checkup.setPassword(connection.split("\\|")[3]);
                ambiente="Test";
            }else
            if(parametros.split(" ")[1].equalsIgnoreCase("weblogic_uat")) {
                connection = loadProperties.getDeploy("weblogic_tes");
                checkup.setHost(connection.split("\\|")[0]);
                checkup.setPort(connection.split("\\|")[1]);
                checkup.setUsername(connection.split("\\|")[2]);
                checkup.setPassword(connection.split("\\|")[3]);
                ambiente="UAT";

            }

            String path=loadProperties.getGeneral("wars").concat("/").concat(parametros.split(" ")[2]).concat("/").concat(parametros.split(" ")[3]);

            ByteArrayOutputStream byteArrayOutputStream=sshBusiness.receiveFileSCPByte(checkup, path);

            connection = loadProperties.getDeploy(parametros.split(" ")[1]);
            checkup=new Checkup();
            checkup.setHost(connection.split("\\|")[0]);
            checkup.setPort(connection.split("\\|")[1]);
            checkup.setUsername(connection.split("\\|")[2]);
            checkup.setPassword(connection.split("\\|")[3]);

            Script script=new Script();
            script.setFile(Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray()));
            checkup.setScript(new ArrayList<>());
            checkup.getScript().add(script);

            path=loadProperties.getGeneral("repositorywar").concat(parametros.split(" ")[3]);

            sshBusiness.sendFileSCPByte(checkup, path);

            Out out=new Out();
            out.setUser(update.getCallbackQuery().getFrom().getFirstName().concat(" ").concat(update.getCallbackQuery().getFrom().getLastName()));
            out.setName("pase".concat(ambiente));
            out.setRecipients(Arrays.asList(loadProperties.getGeneral("mail").split(";")));
            out.setTitle("Solicitud pase a ".concat(ambiente));
            out.setMessageBody(loadProperties.getGeneral("message_body"));
            out.setMessageBody(out.getMessageBody().concat(" ").concat("Solicitud de pase a ").concat(ambiente));
            HashMap<String,byte[]> evidencia= new HashMap<>();
            evidencia.put("Pase".concat(ambiente),mensaje.concat("Pase ").concat(ambiente).concat(" del war ").concat(parametros.split(" ")[3]).concat(" enviado").concat(System.lineSeparator()).getBytes());
            out.setFile(evidencia);

            reportBusiness.ReportCore(out);
            mensaje=mensaje.concat("Pase ").concat(ambiente).concat(" del war ").concat(parametros.split(" ")[3]).concat(" enviado");

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return mensaje;
    }

    public String reDeployWeblogicQAUAT(Update update,String parametros) throws IOException {
        final String origen = "WeblogicServices.reDeployWeblogicQAUAT";
        long time;
        String mensaje = "";
        StringBuffer script;
        Response response = null;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            String connection = loadProperties.getDeploy(parametros.split(" ")[1]);

            String war=doFindWar(update,connection,parametros);
            List<String> listWar= Arrays.asList(war.split("\\n"));

            response=new Response();
            if(listWar.size()>1) {
                //HACER DESPLIEGUE
                response = doRedeploy(update, connection, parametros);

                if (response.getResponse().contains("completed")) {
                    //METODO DE BACKUP
                    doBackupWar(connection, parametros, response.getResponse().split(" ")[55].replace("-cluster\n", ""));
                    //MOVER WAR DE SERVER
                    doMoveWar(connection, parametros, response.getResponse().split(" ")[55].replace("-cluster\n", ""));
                }

            }else{
                response.setResponse("El WAR no existe en el Weblogic para realizar su despliegue");
            }

            mensaje=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            mensaje=mensaje.concat("ERROR EN LA EJECUCIÓN ").concat(e.getMessage());
        }

        return mensaje;
    }

    public String deployProperties(Update update,String parametros) throws IOException {
        final String origen = "WeblogicServices.deployProperties";
        long time;

        String mensaje="";
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();
            Checkup checkup = new Checkup();
            String connection;
            String ambiente = "";

            if(parametros.split(" ")[1].equalsIgnoreCase("weblogic_tes")) {
                    connection = loadProperties.getDeploy("weblogic_dev");
                checkup.setHost(connection.split("\\|")[0]);
                checkup.setPort(connection.split("\\|")[1]);
                checkup.setUsername(connection.split("\\|")[2]);
                checkup.setPassword(connection.split("\\|")[3]);
                ambiente="Test";
            }else
            if(parametros.split(" ")[1].equalsIgnoreCase("weblogic_uat")) {
                connection = loadProperties.getDeploy("weblogic_tes");
                checkup.setHost(connection.split("\\|")[0]);
                checkup.setPort(connection.split("\\|")[1]);
                checkup.setUsername(connection.split("\\|")[2]);
                checkup.setPassword(connection.split("\\|")[3]);
                ambiente="UAT";

            }

            String path=loadProperties.getGeneral("repositoryproperties").concat("/").concat(parametros.split(" ")[2]);

            ByteArrayOutputStream byteArrayOutputStream=sshBusiness.receiveFileSCPByte(checkup, path);

            connection = loadProperties.getDeploy(parametros.split(" ")[1]);
            checkup=new Checkup();
            checkup.setHost(connection.split("\\|")[0]);
            checkup.setPort(connection.split("\\|")[1]);
            checkup.setUsername(connection.split("\\|")[2]);
            checkup.setPassword(connection.split("\\|")[3]);

            Script script=new Script();
            script.setFile(Base64.getEncoder().encodeToString(byteArrayOutputStream.toByteArray()));
            checkup.setScript(new ArrayList<>());
            checkup.getScript().add(script);

            path=loadProperties.getGeneral("repositoryproperties").concat("/").concat(parametros.split(" ")[2]);

            sshBusiness.sendFileSCPByte(checkup, path);

            Out out=new Out();
            out.setUser(update.getCallbackQuery().getFrom().getFirstName().concat(" ").concat(update.getCallbackQuery().getFrom().getLastName()));
            out.setName("pase".concat(ambiente));
            out.setRecipients(Arrays.asList(loadProperties.getGeneral("mail").split(";")));
            out.setTitle("Solicitud pase a ".concat(ambiente));
            out.setMessageBody(loadProperties.getGeneral("message_body"));
            out.setMessageBody(out.getMessageBody().concat(" ").concat("Solicitud de pase a ").concat(ambiente));
            HashMap<String,byte[]> evidencia= new HashMap<>();
            evidencia.put("Pase".concat(ambiente),mensaje.concat("Pase ").concat(ambiente).concat(" del properties ").concat(parametros.split(" ")[2]).concat(" enviado").concat(System.lineSeparator()).getBytes());
            out.setFile(evidencia);

            reportBusiness.ReportCore(out);
            mensaje=mensaje.concat("Pase ").concat(ambiente).concat(" del properties ").concat(parametros.split(" ")[2]).concat(" enviado");

            //ELMININAR PROPERTIES DEL PASE
            Script scriptExec=new Script();
            List<Script> scripts=new ArrayList<>();
            String scriptBck = new String();
            scriptBck=scriptBck.concat("rm ").concat(loadProperties.getGeneral("repositoryproperties").concat("/").concat(parametros.split(" ")[2]));
            scriptExec.setName("PROPERTIES");
            scriptExec.setType("BATCH_SCRIPT");
            scriptExec.setFile(scriptBck);
            scriptExec.command=true;
            scripts.add(scriptExec);

            connection = loadProperties.getDeploy("weblogic_dev");
            checkup=new Checkup();
            checkup.setHost(connection.split("\\|")[0]);
            checkup.setPort(connection.split("\\|")[1]);
            checkup.setUsername(connection.split("\\|")[2]);
            checkup.setPassword(connection.split("\\|")[3]);

            List<Checkup> checkups=new ArrayList<>();
            checkup.setHost(connection.split("\\|")[0]);
            checkup.setPort(connection.split("\\|")[1]);
            checkup.setUsername(connection.split("\\|")[2]);
            checkup.setPassword(connection.split("\\|")[3]);
            checkup.setComponent("WEBLOGIC_DOMAIN");
            checkup.setName("FIND");
            checkup.setTitle("FIND ");
            checkup.setScript(scripts);
            checkups.add(checkup);

            Out o=sshBusiness.execute(checkups.get(0));

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            if(e.getMessage().equalsIgnoreCase("No such file"))
            {
                mensaje="el archivo fue movido o no existe por favor verificar la ruta: /u01/app/WAR/repository/properties";
            }
        }

        return mensaje;
    }

}
