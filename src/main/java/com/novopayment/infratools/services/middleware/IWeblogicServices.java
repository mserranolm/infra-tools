package com.novopayment.infratools.services.middleware;

import com.novopayment.infratools.od.Checkup;
import com.novopayment.infratools.od.Response;
import com.novopayment.infratools.od.Script;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

import java.io.IOException;
import java.sql.SQLException;

public interface IWeblogicServices {

    public String deployWeblogic(Update update, String parametros) throws IOException;
    public String undeployWeblogic(Update update,String parametros) throws IOException;
    public String executePy(Update update) throws IOException;
    public String reDeployWeblogic(Update update,String parametros) throws IOException, SQLException;
    public String getListDeploy(Update update) throws IOException;
    public String getListDeployForManaged(Update update,String managed) throws IOException;
    public String getListProperties(Update update,String managed) throws IOException;
    public String getListPropertiesPath(Update update,String managed) throws IOException;
    public String doMoveProperties(Update update,String parametros,String propiedad,String path) throws IOException;
    public String deployWeblogicPre(Update update, String parametros) throws IOException;
    public String reDeployWeblogicQAUAT(Update update,String parametros) throws IOException;
    public String deployProperties(Update update,String parametros) throws IOException;
}
