package com.novopayment.infratools.services.general;

import org.telegram.telegrambots.api.objects.Update;

import java.io.IOException;
import java.sql.SQLException;

public interface IGeneralServices {

    public String getCredentials(Update update) throws IOException, SQLException;
    public String getListCredential(Update update) throws IOException, SQLException;
    public String getConsoleCredentials(Update update) throws IOException, SQLException;
    public String getListConsoleCredential(Update update) throws IOException, SQLException;
    public String getUser_auth(Update update) throws IOException, SQLException;
    public String getListUser_auth(Update update) throws IOException, SQLException;
    public String getUser_login(Update update) throws IOException, SQLException;
}
