package com.novopayment.infratools.services.general.impl;

import com.novopayment.infratools.core.impl.LoadProperties;
import com.novopayment.infratools.od.Response;
import com.novopayment.infratools.services.general.IGeneralServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.api.objects.Update;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Logger;

@Slf4j
@Service
public class GeneralServices implements IGeneralServices {
    private LoadProperties loadProperties;

    @Autowired
    public GeneralServices(LoadProperties loadProperties) {
        this.loadProperties=loadProperties;
    }

    public String getCredentials(Update update) throws IOException, SQLException {
        final String origen = "GeneralBusiness.getCredentials";
        long time;
        Response response;

        String message="";
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(loadProperties.getCredentials(update.getMessage().getText().split(" ")[1]));
            message=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }


    public String getListCredential(Update update) throws IOException, SQLException {
        final String origen = "GeneralBusiness.getListCredential";
        long time;
        Response response;

        String message="";
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(loadProperties.getListCredentials());
            message=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public String getConsoleCredentials(Update update) throws IOException, SQLException {
        final String origen = "GeneralBusiness.getConsoleCredentials";
        long time;
        Response response;

        String message="";
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(loadProperties.getConsoleCredentials(update.getMessage().getText().split(" ")[1]));

            message=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }


    public String getListConsoleCredential(Update update) throws IOException, SQLException {
        final String origen = "GeneralBusiness.getListConsoleCredential";
        long time;
        Response response;

        String message="";
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(loadProperties.getListConsoleCredentials());

            message=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public String getUser_auth(Update update) throws IOException, SQLException {
        final String origen = "GeneralBusiness.getUser_auth";
        long time;
        Response response;

        String message="";
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(loadProperties.getUser_auth(update.getMessage().getText().split(" ")[1]));

            message=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public String getUser_login(Update update) throws IOException, SQLException {
        final String origen = "GeneralBusiness.getUser_login";
        long time;
        Response response;

        String message="";
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            String bot=loadProperties.getBot_auth(String.valueOf(update.getMessage().getChatId()));

            String user;
            if(!bot.isEmpty()) {
                 user = loadProperties.getUser_auth(String.valueOf(update.getMessage().getFrom().getId()));
                if((user!=null)&&(!user.isEmpty())){
                    response.setResponse(user);
                }
                else{
                    response.setResponse(null);
                }
            }else{
                user = loadProperties.getUser_auth(String.valueOf(update.getMessage().getFrom().getId()));
                response.setResponse(user);
            }

            message=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }


    public String getListUser_auth(Update update) throws IOException, SQLException {
        final String origen = "GeneralBusiness.getListUser_auth";
        long time;
        Response response;

        String message="";
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(loadProperties.getListUser_auth());

            message=response.getResponse();

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

}
