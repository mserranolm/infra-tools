package com.novopayment.infratools.chatbots.impl;

import com.novopayment.infratools.chatbots.IIntegratorChatBootsBusiness;
import com.novopayment.infratools.core.impl.LoadProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Service
public class TelegramCoreUat {
    IIntegratorChatBootsBusiness integratorChatBootsBusiness;

    @Autowired
    public TelegramCoreUat(IIntegratorChatBootsBusiness integratorChatBootsBusiness, LoadProperties loadProperties) {
        this.integratorChatBootsBusiness=integratorChatBootsBusiness;
    }

    public static SendMessage init(SendMessage message, String mensaje){

        if (mensaje!=null) {
            message = message.setText(mensaje.concat("\n").concat("Que acción desea realizar: "));
        }else
        {
            message = message.setText("Que acción desea realizar: ");
        }

        message.setReplyMarkup(showListWarOptions());
        return message;
    }


    public static InlineKeyboardMarkup showListWar(List <String> lista,String accion){

        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();

        int j=0;
        while (j<lista.size()){
            rowInline = new ArrayList<>();
            for (int i = 0; i < 2; i++) {
                InlineKeyboardButton inlineKeyboardButton=new InlineKeyboardButton();
                if(j<lista.size()) {
                    inlineKeyboardButton.setText(lista.get(j)).setCallbackData(lista.get(j).concat(":").concat(accion).concat(":2"));
                    rowInline.add(inlineKeyboardButton);
                }
                j++;
            }
            rowsInline.add(rowInline);
        }


        markupInline.setKeyboard(rowsInline);

        return markupInline;
    }

    public static List<InlineKeyboardMarkup> showListWarSize(List <String> lista,String accion){
        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<InlineKeyboardMarkup> markupInlines = new ArrayList<>();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();

        int j = 0;
        int x = 0;
        while (j < lista.size()) {
            rowInline = new ArrayList<>();
            for (int i = 0; i < 2; i++) {
                InlineKeyboardButton inlineKeyboardButton = new InlineKeyboardButton();
                if (j < lista.size()) {
                    inlineKeyboardButton.setText(lista.get(j)).setCallbackData(lista.get(j).concat(":").concat(accion).concat(":2"));
                    rowInline.add(inlineKeyboardButton);
                }
                j++;x++;
            }
            rowsInline.add(rowInline);
            if(x==20){
                markupInline.setKeyboard(rowsInline);
                markupInlines.add(markupInline);
                markupInline = new InlineKeyboardMarkup();
                rowsInline = new ArrayList<>();
                x=0;
            }
        }

        return markupInlines;
    }

    public static InlineKeyboardMarkup showLisProperties(List <String> lista,String accion){

        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();


        int j=0;
        while (j<lista.size()){
            rowInline = new ArrayList<>();
            for (int i = 0; i < 2; i++) {
                InlineKeyboardButton inlineKeyboardButton=new InlineKeyboardButton();
                if(j<lista.size()) {
                    inlineKeyboardButton.setText(lista.get(j)).setCallbackData("weblogic_uat:".concat(accion.concat(":").concat(lista.get(j))));
                    rowInline.add(inlineKeyboardButton);
                }
                j++;
            }
            rowsInline.add(rowInline);
        }


        markupInline.setKeyboard(rowsInline);

        return markupInline;
    }

    public static InlineKeyboardMarkup showListWarOptions(){

        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();

        rowInline.add(new InlineKeyboardButton().setText("Redesplegar").setCallbackData("weblogic_uat:Redesplegar"));
        rowInline.add(new InlineKeyboardButton().setText("Desplegar").setCallbackData("weblogic_uat:Desplegar"));
        rowInline.add(new InlineKeyboardButton().setText("Propiedades").setCallbackData("weblogic_uat:Propiedades"));
        rowInline.add(new InlineKeyboardButton().setText("Undeploy").setCallbackData("weblogic_uat:Undeploy"));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);

        return markupInline;
    }


    public static InlineKeyboardMarkup showListConfirmarOptions(String nameWar, String accion,String manejado){

        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();

        List<InlineKeyboardButton> rowInline = new ArrayList<>();

        rowInline.add(new InlineKeyboardButton().setText("Si").setCallbackData(nameWar.concat(":").concat(accion).concat(":").concat(manejado)));
        rowInline.add(new InlineKeyboardButton().setText("No").setCallbackData("weblogic_uat".concat(":").concat("no").concat(accion)));
        rowsInline.add(rowInline);
        markupInline.setKeyboard(rowsInline);

        return markupInline;
    }

    public static InlineKeyboardMarkup showListManagedOptions(String nameWar,String managedservers,String accion){

        List<String> ms= Arrays.asList(managedservers.split(","));

        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();

        int j=0;
        while (j<ms.size()){
            rowInline = new ArrayList<>();
            for (int i = 0; i < 2; i++) {
                InlineKeyboardButton inlineKeyboardButton=new InlineKeyboardButton();
                if(j<ms.size()) {
                    inlineKeyboardButton.setText(ms.get(j)).setCallbackData(nameWar.concat(":").concat(accion).concat(":").concat(ms.get(j)));
                    rowInline.add(inlineKeyboardButton);
                }
                j++;
            }
            rowsInline.add(rowInline);
        }
        markupInline.setKeyboard(rowsInline);

        return markupInline;
    }

    public static InlineKeyboardMarkup showListPropertiesOptions(String nmeProperties,List<String> properties,String accion,String file){

        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();
        List<InlineKeyboardButton> rowInline = new ArrayList<>();

        int j=0;
        while (j<properties.size()){
            rowInline = new ArrayList<>();
            for (int i = 0; i < 2; i++) {
                InlineKeyboardButton inlineKeyboardButton=new InlineKeyboardButton();
                if(j<properties.size()) {
                    inlineKeyboardButton.setText(String.valueOf(j+1)).setCallbackData(nmeProperties.concat(":").concat(accion).concat(":").concat(file).concat(":").concat(String.valueOf(j+1)).concat(":"));
                    rowInline.add(inlineKeyboardButton);
                }
                j++;
            }
            rowsInline.add(rowInline);
        }
        markupInline.setKeyboard(rowsInline);

        return markupInline;
    }

    public static SendMessage salir(Update update){
        SendMessage message;

        long chat_id = update.getCallbackQuery().getMessage().getChatId();
        String user_first_name = update.getCallbackQuery().getFrom().getFirstName();
        String user_last_name = update.getCallbackQuery().getFrom().getLastName();

        message = new SendMessage() // Create a message object object
                .setChatId(chat_id)
                .setText("Señor "+user_first_name+" " +user_last_name+" ya salio de la navegación de InfraTools");

        message.enableMarkdown(true);

        return message;
    }



}
