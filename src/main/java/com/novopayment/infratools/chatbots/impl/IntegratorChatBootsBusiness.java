package com.novopayment.infratools.chatbots.impl;

import com.novopayment.infratools.chatbots.IIntegratorChatBootsBusiness;
import com.novopayment.infratools.core.impl.IntegratorBusiness;
import com.novopayment.infratools.od.Checkup;
import com.novopayment.infratools.od.Response;
import com.novopayment.infratools.services.database.IOracleService;
import com.novopayment.infratools.services.database.impl.OracleService;
import com.novopayment.infratools.services.general.IGeneralServices;
import com.novopayment.infratools.services.middleware.IWeblogicServices;
import com.novopayment.infratools.services.so.ISoServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;


import java.io.IOException;
import java.sql.SQLException;
import java.util.List;
import java.util.logging.Logger;


@Slf4j
@Service
public class IntegratorChatBootsBusiness implements IIntegratorChatBootsBusiness {
    IntegratorBusiness integratorBusiness;
    IWeblogicServices weblogicServices;
    ISoServices soServices;
    IGeneralServices generalServices;
    IOracleService oracleService;

    @Autowired
    public IntegratorChatBootsBusiness(IntegratorBusiness integratorBusiness, IWeblogicServices weblogicServices, ISoServices soServices, IGeneralServices generalServices, OracleService oracleService) {
        this.integratorBusiness=integratorBusiness;
        this.weblogicServices=weblogicServices;
        this.soServices=soServices;
        this.generalServices=generalServices;
        this.oracleService=oracleService;
    }


    public static SendMessage putMessage(Update update,String response){

        SendMessage message=null;

        long chat_id;
        if(update.getCallbackQuery()!=null)
            chat_id = update.getCallbackQuery().getMessage().getChatId();
        else
            chat_id = update.getMessage().getChatId();

        message = new SendMessage() // Create a message object object
                .setChatId(chat_id)
                .setText(response);
        return message;

    }

    public SendMessage getListExecute(Update update) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.getListExecute";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(soServices.getListExecute(update));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public SendMessage execute(Update update) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.execute";
        long time;
        List<Checkup> checkups;
        Checkup checkup;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(soServices.execute(update));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }


    public SendMessage getCredentials(Update update) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.getCredentials";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(generalServices.getCredentials(update));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }


    public SendMessage getListCredential(Update update) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.getListCredential";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(generalServices.getListCredential(update));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public SendMessage getConsoleCredentials(Update update) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.getConsoleCredentials";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(generalServices.getConsoleCredentials(update));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }


    public SendMessage getListConsoleCredential(Update update) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.getListConsoleCredential";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(generalServices.getListConsoleCredential(update));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public SendMessage deployWeblogic(Update update, String parametros) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.deployWeblogic";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(weblogicServices.deployWeblogic(update,parametros));

            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }


        public SendMessage undeployWeblogic(Update update,String parametros) throws IOException, SQLException {
            final String origen = "IntegratorBusiness.undeployWeblogic";
            long time;
            Response response;

            SendMessage message=null;
            try {
                log.info("Iniciando | " + origen);
                time = System.currentTimeMillis();

                response=new Response();
                response.setResponse(weblogicServices.undeployWeblogic(update,parametros));
                message=putMessage(update,response.getResponse());

                time = System.currentTimeMillis() - time;
                log.info("Iniciando | " + origen+ " " + time);
            } catch (Exception e) {
                log.error("Failed to Execute, The error is" + e.getMessage());
            }

            return message;
        }

    public SendMessage reDeployWeblogic(Update update,String parametros) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.redeployWeblogic";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(weblogicServices.reDeployWeblogic(update,parametros));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public SendMessage executePy(Update update) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.executePy";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(weblogicServices.executePy(update));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public SendMessage getUser_auth(Update update) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.getUser_auth";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(generalServices.getUser_auth(update));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }


    public SendMessage getListUser_auth(Update update) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.getListUser_auth";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(generalServices.getListUser_auth(update));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public SendMessage login(Update update) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.login";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(generalServices.getUser_login(update));
            if(response.getResponse()==null) {
                message = putMessage(update, "El usuario id ".concat(String.valueOf(update.getMessage().getFrom().getId())).concat(" No puede ejecutar este comando"));
                log.info("Usuario id ".concat(String.valueOf(update.getMessage().getFrom().getId()).concat(" NO Autenticado")));
            }
            else {
                message = putMessage(update, "Usuario puede ejectuar el comando");
                log.info("Usuario id ".concat(String.valueOf(update.getMessage().getFrom().getId()).concat(" Autenticado")));
            }

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public SendMessage waitDb(Update update) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.waitDb";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(oracleService.execute(update));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public SendMessage getListDeploy(Update update) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.getListDeploy";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(weblogicServices.getListDeploy(update));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public SendMessage getListDeployForManaged(Update update,String managed) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.getListDeployForManaged";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(weblogicServices.getListDeployForManaged(update,managed));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public SendMessage getListProperties(Update update,String managed) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.getListProperties";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(weblogicServices.getListProperties(update,managed));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }
    public SendMessage getListPropertiesPath(Update update,String managed) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.getListPropertiesPath";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(weblogicServices.getListPropertiesPath(update,managed));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public SendMessage doMoveProperties(Update update,String parametros,String propiedad, String path) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.doMoveProperties";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(weblogicServices.doMoveProperties(update,parametros,propiedad,path));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }


    public SendMessage deployWeblogicPre(Update update, String parametros) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.deployWeblogicPre";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(weblogicServices.deployWeblogicPre(update,parametros));

            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }


    public SendMessage reDeployWeblogicQAUAT(Update update,String parametros) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.reDeployWeblogicQAUAT";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(weblogicServices.reDeployWeblogic(update,parametros));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

    public SendMessage deployProperties(Update update,String parametros) throws IOException, SQLException {
        final String origen = "IntegratorBusiness.deployProperties";
        long time;
        Response response;

        SendMessage message=null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            response=new Response();
            response.setResponse(weblogicServices.deployProperties(update,parametros));
            message=putMessage(update,response.getResponse());

            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return message;
    }

}
