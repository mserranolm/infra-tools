package com.novopayment.infratools.chatbots.impl;

import com.novopayment.infratools.chatbots.IIntegratorChatBootsBusiness;
import com.novopayment.infratools.core.impl.LoadProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;
import java.io.IOException;
import java.sql.SQLException;

import static com.novopayment.infratools.chatbots.impl.TelegramCore.init;
import static com.novopayment.infratools.chatbots.impl.TelegramCore.salir;

@Slf4j
public class TelegramBots extends TelegramLongPollingBot {
    IIntegratorChatBootsBusiness integratorChatBootsBusiness;
    private LoadProperties loadProperties;

    @Autowired
    public TelegramBots(IIntegratorChatBootsBusiness integratorChatBootsBusiness,LoadProperties loadProperties) {
        this.integratorChatBootsBusiness=integratorChatBootsBusiness;
        this.loadProperties=loadProperties;
    }

    @Override
    public void onUpdateReceived(Update update) {
        SendMessage message = null;
        boolean login = false;

        try {

            if(update.hasMessage()) {
                if (update.hasMessage() && update.getMessage().hasText()) {

                    message=integratorChatBootsBusiness.login(update);
                    if(!message.getText().contains("no puede ejecutar este comando")) {
                        long chat_id = update.getMessage().getChatId();
                        String response = this.validInput(update.getMessage().getText());
                        message = new SendMessage() // Create a message object object
                                    .setChatId(chat_id)
                                .setText(response);


                        if (response.isEmpty()) {
                            if (update.getMessage().getText().contains("-exec")) {
                                if (update.getMessage().getText().split(" ")[1].equalsIgnoreCase("list"))
                                    message = integratorChatBootsBusiness.getListExecute(update);
                                else
                                    message = integratorChatBootsBusiness.execute(update);
                            } else if (update.getMessage().getText().contains("-credential")) {
                                if (update.getMessage().getText().split(" ")[1].equalsIgnoreCase("list"))
                                    message = integratorChatBootsBusiness.getListCredential(update);
                                else
                                    message = integratorChatBootsBusiness.getCredentials(update);
                            } else if (update.getMessage().getText().contains("-console_credential")) {
                                if (update.getMessage().getText().split(" ")[1].equalsIgnoreCase("list"))
                                    message = integratorChatBootsBusiness.getListConsoleCredential(update);
                                else
                                    message = integratorChatBootsBusiness.getConsoleCredentials(update);
                            } else if (update.getMessage().getText().contains("-deploy")) {
                                if (update.getMessage().getText().split(" ")[1].equalsIgnoreCase("list"))
                                    message = integratorChatBootsBusiness.deployWeblogic(update,update.getMessage().getText());
                                else
                                    message = integratorChatBootsBusiness.deployWeblogic(update,update.getMessage().getText());
                            }else if (update.getMessage().getText().contains("-undeploy")) {
                                if (update.getMessage().getText().split(" ")[1].equalsIgnoreCase("list"))
                                    message = integratorChatBootsBusiness.undeployWeblogic(update,update.getMessage().getText());
                                else
                                    message = integratorChatBootsBusiness.undeployWeblogic(update,update.getMessage().getText());
                            }else if (update.getMessage().getText().contains("-redeploy")) {
                                if (update.getMessage().getText().split(" ")[1].equalsIgnoreCase("list"))
                                    message = integratorChatBootsBusiness.reDeployWeblogic(update,update.getMessage().getText());
                                else
                                    message = integratorChatBootsBusiness.reDeployWeblogic(update,update.getMessage().getText());
                            }
                            else if (update.getMessage().getText().contains("-user")) {
                                if (update.getMessage().getText().split(" ")[1].equalsIgnoreCase("list"))
                                    message = integratorChatBootsBusiness.getListUser_auth(update);
                                else
                                    message = integratorChatBootsBusiness.getUser_auth(update);
                            }
                            else if (update.getMessage().getText().contains("-db")) {
                                if (update.getMessage().getText().split(" ")[1].equalsIgnoreCase("wait"))
                                    message = integratorChatBootsBusiness.waitDb(update);
                                else
                                    message = integratorChatBootsBusiness.getUser_auth(update);
                            }
                            if (update.getMessage().getText().contains("-review")) {
                                message = integratorChatBootsBusiness.executePy(update);
                            } else if (update.getMessage().getText().contains("init")) {
                                message = init(update);
                            }
                        }
                    }
                }


            }else
            if(update.hasCallbackQuery()) {
                CallbackQuery callbackquery = update.getCallbackQuery();
                String[] data = callbackquery.getData().split(":");
                String[] options=data[2].split("_");

                String option=options[0];
                switch(option){
                    case "menu":
                        int index = Integer.parseInt(options[1]);
                        switch (index) {
                            case 1:
                                //message=integratorChatBootsBusiness.executeTest(update);
                                break;
                            /*case 2:
                                message=integratorChatBootsBusiness.getListProperties(update);
                                break;*/
                            case 5:
                                message=salir(update);
                                break;
                        }
                        break;
                    case "client":
                        //message=integratorChatBootsBusiness.getContactClient(update,options[1]);
                        break;
                    case "checkup":
                        //message=integratorChatBootsBusiness.executeRevisión(update,data[2].substring(data[2].indexOf("_")+1,data[2].length()));
                        break;
                }
            }

            if(!message.getText().isEmpty())
                execute(message); // Sending our message object to user


        } catch (TelegramApiException e) {
            log.error(e.getMessage());
        } catch (SQLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    public String validInput(String input) {
        String response = "";

        if (input.contains("-")) {
            if (input.contains("-deploy")) {
                if (input.split(" ").length != 6 && input.split(" ").length != 2) {
                    response = "Cantidad de parametros para -deploy invalidos, el comando espera 5 y se estan enviando ".concat(String.valueOf(input.split(" ").length));
                    response = response.concat(System.lineSeparator()).concat(" La forma de ejecutar este comando es: -deploy ambiente manejado nombre_Wart ticket correo");
                    response = response.concat(System.lineSeparator()).concat(" Ejemplo: -deploy weblogic_tes banco-bogota-server hello-world.war T-0000000 infra@novopayment.com");
                }
            } else if (input.contains("-credential")) {
                response = "";
            } else if (input.contains("-exec")) {
                response = "";
            } else if (input.contains("-console_credential")) {
                response = "";
            } else if (input.contains("-review")) {
                response = "";
            } else if (input.contains("-undeploy")) {
                response = "";
            }
              else if (input.contains("-redeploy")) {
                response = "";
            } else if (input.contains("-user")) {
                response = "";
            }else if (input.contains("-db")) {
                response = "";
            } else {
                response = "Comando no reconocido";
            }

        }


            return response;
        }


    @Override
    public String getBotUsername() {
        // TODO
        return loadProperties.getGeneral("botusername");
    }

    @Override
    public String getBotToken() {
        // TODO
        return loadProperties.getGeneral("bottoken");
    }


}
