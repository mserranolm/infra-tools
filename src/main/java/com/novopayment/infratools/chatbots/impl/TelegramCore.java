package com.novopayment.infratools.chatbots.impl;

import com.novopayment.infratools.chatbots.IIntegratorChatBootsBusiness;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.buttons.InlineKeyboardButton;

import java.util.ArrayList;
import java.util.List;

@Slf4j
@Service
public class TelegramCore {
    IIntegratorChatBootsBusiness integratorChatBootsBusiness;

    @Autowired
    public TelegramCore(IIntegratorChatBootsBusiness integratorChatBootsBusiness) {
        this.integratorChatBootsBusiness=integratorChatBootsBusiness;
    }

    public static SendMessage init(Update update){
        SendMessage message = null;

        // We check if the update has a message and the message has text
        if (update.hasMessage() && update.getMessage().hasText()) {
            // Set variables
            String message_text = update.getMessage().getText();
            long chat_id = update.getMessage().getChatId();

            String user_first_name = update.getMessage().getFrom().getFirstName();
            String user_last_name = update.getMessage().getFrom().getLastName();
            long user_id = update.getMessage().getFrom().getId();

            message = new SendMessage() // Create a message object object
                    .setChatId(chat_id)
                    .setText("Bienvenidos "+user_first_name+" " +user_last_name+" A la versión Beta InfraTools, Eliga la opción deseada");


            message.setReplyMarkup(showOptions());
        }

        return message;
    }


    public static InlineKeyboardMarkup showOptions(){

        InlineKeyboardMarkup markupInline = new InlineKeyboardMarkup();
        List<List<InlineKeyboardButton>> rowsInline = new ArrayList<>();

        List<InlineKeyboardButton> rowInline0 = new ArrayList<>();
        rowInline0.add(new InlineKeyboardButton().setText("Test").setCallbackData("gallery:text:menu_1"));

        List<InlineKeyboardButton> rowInline1 = new ArrayList<>();
        rowInline1.add(new InlineKeyboardButton().setText("Lista Propiedades").setCallbackData("gallery:text:menu_2"));

        /* List<InlineKeyboardButton> rowInline2 = new ArrayList<>();
        rowInline2.add(new InlineKeyboardButton().setText("Administración").setCallbackData("gallery:text:menu_3"));

        List<InlineKeyboardButton> rowInline3 = new ArrayList<>();
        rowInline3.add(new InlineKeyboardButton().setText("Revisión").setCallbackData("gallery:text:menu_4"));

        List<InlineKeyboardButton> rowInline4 = new ArrayList<>();
        rowInline4.add(new InlineKeyboardButton().setText("Salir").setCallbackData("gallery:text:menu_5"));*/

        rowsInline.add(rowInline0);
        rowsInline.add(rowInline1);
/*        rowsInline.add(rowInline2);
        rowsInline.add(rowInline3);
        rowsInline.add(rowInline4);*/

        markupInline.setKeyboard(rowsInline);

        return markupInline;
    }


    public static SendMessage salir(Update update){
        SendMessage message;

        long chat_id = update.getCallbackQuery().getMessage().getChatId();
        String user_first_name = update.getCallbackQuery().getFrom().getFirstName();
        String user_last_name = update.getCallbackQuery().getFrom().getLastName();

        message = new SendMessage() // Create a message object object
                .setChatId(chat_id)
                .setText("Señor "+user_first_name+" " +user_last_name+" ya salio de la navegación de Vigilant");

        message.enableMarkdown(true);

        return message;
    }



}
