package com.novopayment.infratools.chatbots.impl;

import com.google.gson.Gson;
import com.novopayment.infratools.chatbots.INotificationChatBoot;
import com.novopayment.infratools.core.impl.LoadProperties;
import com.novopayment.infratools.od.RequestNotified;
import com.novopayment.infratools.od.Response;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;

@RestController
public class NotificationChatBoot implements INotificationChatBoot {
    private LoadProperties loadProperties;

    @Autowired
    public NotificationChatBoot(LoadProperties loadProperties) {
        this.loadProperties=loadProperties;
    }

    public void sendNotified(String notified) {

        try {

            String urlString = loadProperties.getGeneral("urlapitelegram");
            String apiToken = loadProperties.getGeneral("bottoken");
            String chatId = loadProperties.getGeneral("chatid");

            urlString = String.format(urlString, apiToken, chatId, notified.replace(" ","%20"));

            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();

            StringBuilder sb = new StringBuilder();
            InputStream is = new BufferedInputStream(conn.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = "";
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }
            String response = sb.toString();


        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public void sendNotifiedJson(String notified){

        String urlString = loadProperties.getGeneral("urlapitelegrampost");
        RequestNotified requestNotified = new RequestNotified();
        requestNotified.setApiToken(loadProperties.getGeneral("bottoken"));
        requestNotified.setChat_id(loadProperties.getGeneral("chatid"));
        requestNotified.setText(notified);

        try {

            urlString = String.format(urlString, requestNotified.getApiToken());

            URL url = new URL(urlString);
            HttpURLConnection conn = null;

            conn = (HttpURLConnection) url.openConnection();

            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");

            OutputStream os = conn.getOutputStream();
            Gson g = new Gson();
            requestNotified.setApiToken(null);
            String jsonString = g.toJson(requestNotified);

            os.write(jsonString.getBytes("UTF-8"));
            os.close();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = org.apache.commons.io.IOUtils.toString(in, "UTF-8");

            in.close();
            conn.disconnect();


        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    @PostMapping("/sendNotified")
    public Response sendNotified(@RequestBody RequestNotified notified) {
        Response response = null;

        try {

            String urlString = loadProperties.getGeneral("urlapitelegram");
            String apiToken = notified.getApiToken();
            String chatId = notified.getChat_id();

            urlString = String.format(urlString, apiToken, chatId, notified.getText()   .replace(" ","%20"));

            URL url = new URL(urlString);
            URLConnection conn = url.openConnection();

            StringBuilder sb = new StringBuilder();
            InputStream is = new BufferedInputStream(conn.getInputStream());
            BufferedReader br = new BufferedReader(new InputStreamReader(is));
            String inputLine = "";
            while ((inputLine = br.readLine()) != null) {
                sb.append(inputLine);
            }

            response=new Response();
            response.setResponse(sb.toString());
            response.setCode("0");

        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }


    @PostMapping("/sendNotifiedPost")
    public Response sendNotifiedJson(@RequestBody RequestNotified notified) {
        Response response = null;

        try {

            String urlString = loadProperties.getGeneral("urlapitelegrampost");
            String apiToken = notified.getApiToken();
            urlString = String.format(urlString, apiToken);

            URL url = new URL(urlString);
            HttpURLConnection conn = null;

            conn = (HttpURLConnection) url.openConnection();

            conn.setConnectTimeout(5000);
            conn.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            conn.setDoOutput(true);
            conn.setDoInput(true);
            conn.setRequestMethod("POST");

            OutputStream os = conn.getOutputStream();
            Gson g = new Gson();
            notified.setApiToken(null);
            String jsonString = g.toJson(notified);

            os.write(jsonString.getBytes("UTF-8"));
            os.close();

            // read the response
            InputStream in = new BufferedInputStream(conn.getInputStream());
            String result = org.apache.commons.io.IOUtils.toString(in, "UTF-8");

            in.close();
            conn.disconnect();

            response.setResponse(result);
            response.setCode(String.valueOf(0));

        } catch (IOException e) {
            e.printStackTrace();
        }

        return response;
    }



}
