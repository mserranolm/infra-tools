package com.novopayment.infratools.chatbots.impl;

import com.novopayment.infratools.chatbots.IIntegratorChatBootsBusiness;
import com.novopayment.infratools.core.impl.LoadProperties;
import com.novopayment.infratools.od.Response;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.CallbackQuery;
import org.telegram.telegrambots.api.objects.Message;
import org.telegram.telegrambots.api.objects.Update;
import org.telegram.telegrambots.api.objects.replykeyboard.InlineKeyboardMarkup;
import org.telegram.telegrambots.api.objects.replykeyboard.ReplyKeyboard;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.exceptions.TelegramApiException;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.novopayment.infratools.chatbots.impl.TelegramCoreDev.*;

@Slf4j
public class TelegramBotsDev extends TelegramLongPollingBot {
    IIntegratorChatBootsBusiness integratorChatBootsBusiness;
    private LoadProperties loadProperties;

    @Autowired
    public TelegramBotsDev(IIntegratorChatBootsBusiness integratorChatBootsBusiness, LoadProperties loadProperties) {
        this.integratorChatBootsBusiness=integratorChatBootsBusiness;
        this.loadProperties=loadProperties;
    }

    @Override
    public void onUpdateReceived(Update update) {
        List<SendMessage> messages = new ArrayList<>();
        boolean login = false;

        try {

            if(update.hasMessage()) {
                messages.add(writeRequest(update));
            }else
            if(update.hasCallbackQuery()) {
                messages=clickRequest(update);
            }

            for(SendMessage sendMessage:messages){
                execute(sendMessage); // Sending our message object to user
            }

        } catch (TelegramApiException e) {
            log.error(e.getMessage());
        }

    }

    public String validInput(String input) {
        String response = "";

        if (input.contains("-")) {
        }else if (input.contains("-init")) {
            response = "";
        }
        else {
            response = "Comando no reconocido";
        }


        return response;
    }


    @Override
    public String getBotUsername() {
        // TODO
        return loadProperties.getGeneral("botusernamedev");
    }

    @Override
    public String getBotToken() {
        // TODO
        return loadProperties.getGeneral("bottokendev");
    }

    public SendMessage writeRequest(Update update){
        SendMessage message = null;

        try {
            message=integratorChatBootsBusiness.login(update);
            if (update.hasMessage() && update.getMessage().hasText()) {
                if (!message.getText().contains("No puede ejecutar este comando")) {
                    long chat_id = update.getMessage().getChatId();
                    String response = this.validInput(update.getMessage().getText());
                    message = new SendMessage() // Create a message object object
                            .setChatId(chat_id)
                            .setText(response);

                    if (response.isEmpty()) {
                        if (update.getMessage().getText().contains("-init")) {
                            message=init(message,null);
                        }
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return message;
    }

    public List<SendMessage> clickRequest(Update update){
        ArrayList<SendMessage> sendMessages=new ArrayList<>();
        SendMessage message = null;
        CallbackQuery callbackquery = update.getCallbackQuery();
        String[] data = callbackquery.getData().split(":");
        List<String> listaWar;
        List<String> listProperties;

        try {

            switch(data[1]) {
                case "Desplegar" :
                    message = integratorChatBootsBusiness.getListDeploy(update);
                    if(message.getText()!=null) {
                        listaWar = Arrays.asList(message.getText().split("\\n"));
                        message.setText("WAR disponibles para su despliegue");
                        message.setReplyMarkup(showListWar(listaWar, data[1].concat("+")));
                    }else{
                        message.setText("No hay WARs disponibles en el repositorio");
                    }
                    sendMessages.add(message);
                    break;
                case "Desplegar+" :
                    message = IntegratorChatBootsBusiness.putMessage(update, "Que servidor manejado desea desplegar: ");
                    message=message.setReplyMarkup(showListManagedOptions(data[0],loadProperties.getGeneral("managedservers"),"Desplegar++"));
                    sendMessages.add(message);
                    break;
                case "Desplegar++" :
                    message = IntegratorChatBootsBusiness.putMessage(update, "Esta seguro que este es el war a desplegar ".concat(data[0]).concat(" en el manejado ").concat(data[2]));
                    message.setReplyMarkup(showListConfirmarOptions(data[0],"Desplegar+++",data[2]));
                    sendMessages.add(message);
                    break;
                case "Desplegar+++" :
                    String deploy="-deploy weblogic_dev ".concat(data[2]).concat(" ").concat(data[0]);
                    message = integratorChatBootsBusiness.deployWeblogic(update,deploy);
                    sendMessages.add(message);
                    break;
                case "Undeploy" :
                    message = IntegratorChatBootsBusiness.putMessage(update, "Selecionar servidor manejado donde se encuentra el despliegue");
                    message=message.setReplyMarkup(showListManagedOptions(data[0],loadProperties.getGeneral("managedservers"),"Undeploy+"));
                    sendMessages.add(message);
                    break;
                case "Undeploy+" :
                    listaWar= Arrays.asList(integratorChatBootsBusiness.getListDeployForManaged(update,data[2]).getText().split("\\n"));
                    List<InlineKeyboardMarkup> inlineKeyboardMarkups=showListWarSize(listaWar,data[1].concat("+"));
                    SendMessage msg=new SendMessage();
                    msg=IntegratorChatBootsBusiness.putMessage(update, "Seleccionar el war para hacer undeploy");
                    if(listaWar.size()>100) {
                        for (InlineKeyboardMarkup inlineKeyboardMarkup : inlineKeyboardMarkups) {
                            msg.setReplyMarkup(inlineKeyboardMarkup);
                            sendMessages.add(msg);
                            msg = new SendMessage();
                            msg = IntegratorChatBootsBusiness.putMessage(update, "-------------------------------------------------------------------------------");
                        }
                    }else{
                        msg.setReplyMarkup(showListWar(listaWar,data[1].concat("+")));
                        sendMessages.add(msg);
                    }
                    break;
                case "Undeploy++" :
                    String undeploy="-undeploy weblogic_dev ".concat(data[2]).concat(" ").concat(data[0]);
                    message = integratorChatBootsBusiness.undeployWeblogic(update,undeploy);
                    sendMessages.add(message);
                    break;
                case "Redesplegar" :
                    message = IntegratorChatBootsBusiness.putMessage(update, "Selecionar war al que desea hacer Redeploy");
                    listaWar= Arrays.asList(integratorChatBootsBusiness.getListDeploy(update).getText().split("\n"));
                    message.setReplyMarkup(showListWar(listaWar,data[1].concat("+")));
                    sendMessages.add(message);
                    break;
                case "Redesplegar+" :
                    message = IntegratorChatBootsBusiness.putMessage(update, "Esta seguro que este es el war a desplegar ".concat(data[0]));
                    message.setReplyMarkup(showListConfirmarOptions(data[0],"Redesplegar++",data[1]));
                    sendMessages.add(message);
                    break;
                case "Redesplegar++" :
                    String redeploy="-deploy weblogic_dev ".concat(data[2]).concat(" ").concat(data[0]);
                    message = integratorChatBootsBusiness.reDeployWeblogic(update,redeploy);
                    sendMessages.add(message);
                    break;
                case "Propiedades" :
                    message = integratorChatBootsBusiness.getListProperties(update,data[0].concat("+"));
                    if(message.getText()!=null) {
                        listProperties = Arrays.asList(message.getText().split("\\n"));
                        message.setText("Propiedades disponibles para su despliegue");
                        message.setReplyMarkup(showLisProperties(listProperties, data[1].concat("+")));
                    }else{
                        message.setText("No hay propiedades disponibles en el repositorio");
                    }
                    sendMessages.add(message);
                    break;
                case "Propiedades+" :
                    message = integratorChatBootsBusiness.getListPropertiesPath(update,data[0].concat("++"));
                    if(message.getText()!=null) {
                        listProperties = Arrays.asList(message.getText().split("\\n"));
                        message = message.setText("Path de despliegue\n");
                        int i = 1;
                        for (String path : listProperties) {
                            message = message.setText(message.getText().concat(String.valueOf(i).concat(". ").concat(path.concat("\n"))));
                            i++;
                        }
                        message.setReplyMarkup(showListPropertiesOptions(data[0], listProperties, "Propiedades++", data[2]));
                    }else {
                        message.setText("La propiedad no existe en el directorio de parametros");
                    }
                    sendMessages.add(message);
                    break;
                case "Propiedades++" :
                    message = integratorChatBootsBusiness.doMoveProperties(update,data[0],data[3],data[2]);
                    sendMessages.add(message);
                    break;
                case "PaseAppTEST" :
                    message = IntegratorChatBootsBusiness.putMessage(update, "Selecionar servidor manejado donde se encuentra el war");
                    message=message.setReplyMarkup(showListManagedOptions(data[0],loadProperties.getGeneral("managedservers"),"PaseAppTEST+"));
                    sendMessages.add(message);
                    break;
                case "PaseAppTEST+" :
                    listaWar= Arrays.asList(integratorChatBootsBusiness.getListDeployForManaged(update,data[2]).getText().split("\\n"));
                    inlineKeyboardMarkups=showListWarSizePre(listaWar,data[1].concat("+"),data[2]);
                    msg=new SendMessage();
                    msg=IntegratorChatBootsBusiness.putMessage(update, "Seleccionar el war para solicitar pase Test");
                    if(listaWar.size()>100) {
                        for (InlineKeyboardMarkup inlineKeyboardMarkup : inlineKeyboardMarkups) {
                            msg.setReplyMarkup(inlineKeyboardMarkup);
                            sendMessages.add(msg);
                            msg = new SendMessage();
                            msg = IntegratorChatBootsBusiness.putMessage(update, "-------------------------------------------------------------------------------");
                        }
                    }else{
                        msg.setReplyMarkup(showListWarPre(listaWar,data[1].concat("+"),data[2]));
                        sendMessages.add(msg);
                    }
                    break;
                case "PaseAppTEST++" :
                    String PaseTest="-deploy weblogic_tes ".concat(data[2]).concat(" ").concat(data[0]);
                    message = integratorChatBootsBusiness.deployWeblogicPre(update,PaseTest);
                    sendMessages.add(message);
                    break;
                case "PaseAppUAT" :
                    message = IntegratorChatBootsBusiness.putMessage(update, "Selecionar servidor manejado donde se encuentra el war");
                    message=message.setReplyMarkup(showListManagedOptions(data[0],loadProperties.getGeneral("managedservers"),"PaseAppUAT+"));
                    sendMessages.add(message);
                    break;
                case "PaseAppUAT+" :
                    listaWar= Arrays.asList(integratorChatBootsBusiness.getListDeployForManaged(update,data[2]).getText().split("\\n"));
                    inlineKeyboardMarkups=showListWarSizePre(listaWar,data[1].concat("+"),data[2]);
                    msg=new SendMessage();
                    msg=IntegratorChatBootsBusiness.putMessage(update, "Seleccionar el war para solicitar pase UAT");
                    if(listaWar.size()>100) {
                        for (InlineKeyboardMarkup inlineKeyboardMarkup : inlineKeyboardMarkups) {
                            msg.setReplyMarkup(inlineKeyboardMarkup);
                            sendMessages.add(msg);
                            msg = new SendMessage();
                            msg = IntegratorChatBootsBusiness.putMessage(update, "-------------------------------------------------------------------------------");
                        }
                    }else{
                        msg.setReplyMarkup(showListWarPre(listaWar,data[1].concat("+"),data[2]));
                        sendMessages.add(msg);
                    }
                    break;
                case "PaseAppUAT++" :
                    String PaseUAT="-deploy weblogic_uat ".concat(data[2]).concat(" ").concat(data[0]);
                    message = integratorChatBootsBusiness.deployWeblogicPre(update,PaseUAT);
                    sendMessages.add(message);
                    break;
                case "PasePropTEST" :
                    message = integratorChatBootsBusiness.getListProperties(update,data[0].concat("+"));
                    if(message.getText()!=null) {
                        listProperties = Arrays.asList(message.getText().split("\\n"));
                        message.setText("Propiedades disponibles para su despliegue");
                        message.setReplyMarkup(showLisProperties(listProperties, data[1].concat("+")));
                    }else{
                        message.setText("No hay propiedades disponibles en el repositorio");
                    }
                    sendMessages.add(message);
                    break;
                case "PasePropTEST+" :
                    message = IntegratorChatBootsBusiness.putMessage(update, "Esta seguro que este es el properties a enviar ".concat(data[2]));
                    message.setReplyMarkup(showListConfirmarOptions(data[0],"PasePropTEST++",data[2]));
                    sendMessages.add(message);
                    break;
                case "PasePropTEST++" :
                    String PaseProTest="-deploy weblogic_tes ".concat(data[2]).concat(" ").concat(data[0]);
                    message = integratorChatBootsBusiness.deployProperties(update,PaseProTest);
                    sendMessages.add(message);
                    break;
                case "PasePropUAT" :
                    message = integratorChatBootsBusiness.getListProperties(update,data[0].concat("+"));
                    if(message.getText()!=null) {
                        listProperties = Arrays.asList(message.getText().split("\\n"));
                        message.setText("Propiedades disponibles para su despliegue");
                        message.setReplyMarkup(showLisProperties(listProperties, data[1].concat("+")));
                    }else{
                        message.setText("No hay propiedades disponibles en el repositorio");
                    }
                    sendMessages.add(message);
                    break;
                case "PasePropUAT+" :
                    message = IntegratorChatBootsBusiness.putMessage(update, "Esta seguro que este es el properties a enviar ".concat(data[2]));
                    message.setReplyMarkup(showListConfirmarOptions(data[0],"PasePropUAT++",data[2]));
                    sendMessages.add(message);
                    break;
                case "PasePropUAT++" :
                    String PaseProUat="-deploy weblogic_uat ".concat(data[2]).concat(" ").concat(data[0]);
                    message = integratorChatBootsBusiness.deployProperties(update,PaseProUat);
                    sendMessages.add(message);
                    break;
                case "no" :
                    message = IntegratorChatBootsBusiness.putMessage(update, "Despliegue cancelado, selecionar el despliegue correcto");
                    message = integratorChatBootsBusiness.getListDeploy(update);
                    listaWar= Arrays.asList(message.getText().split("\\n"));
                    message.setText("WAR disponibles para su despliegue");
                    message.setReplyMarkup(showListWar(listaWar,data[2]));
                    sendMessages.add(message);
                    break;

            }

        } catch (IOException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        }

        return sendMessages;
    }
}
