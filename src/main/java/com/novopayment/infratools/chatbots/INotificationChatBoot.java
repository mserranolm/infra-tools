package com.novopayment.infratools.chatbots;

import com.novopayment.infratools.od.RequestNotified;
import com.novopayment.infratools.od.Response;
import org.springframework.web.bind.annotation.RequestBody;

import java.io.IOException;

public interface INotificationChatBoot {
    public void sendNotified(String message) throws IOException, InterruptedException;
    public Response sendNotified(@RequestBody RequestNotified notified) throws IOException, InterruptedException;
    public Response sendNotifiedJson(@RequestBody RequestNotified notified) throws IOException, InterruptedException;
    public void sendNotifiedJson(String notified) throws IOException, InterruptedException;
}
