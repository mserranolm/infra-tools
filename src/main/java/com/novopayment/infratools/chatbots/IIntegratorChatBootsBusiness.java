package com.novopayment.infratools.chatbots;

import org.telegram.telegrambots.api.methods.send.SendMessage;
import org.telegram.telegrambots.api.objects.Update;

import java.io.IOException;
import java.sql.SQLException;

public interface IIntegratorChatBootsBusiness {
    public SendMessage getListExecute(Update update) throws IOException, SQLException;
    public SendMessage execute(Update update) throws IOException, SQLException;
    public SendMessage getCredentials(Update update) throws IOException, SQLException;
    public SendMessage getListCredential(Update update) throws IOException, SQLException;
    public SendMessage getConsoleCredentials(Update update) throws IOException, SQLException;
    public SendMessage getListConsoleCredential(Update update) throws IOException, SQLException;
    public SendMessage deployWeblogic(Update update, String parametros) throws IOException, SQLException;
    public SendMessage undeployWeblogic(Update update,String parametros) throws IOException, SQLException;
    public SendMessage executePy(Update update) throws IOException, SQLException;
    public SendMessage getUser_auth(Update update) throws IOException, SQLException;
    public SendMessage getListUser_auth(Update update) throws IOException, SQLException;
    public SendMessage login(Update update) throws IOException, SQLException;
    public SendMessage reDeployWeblogic(Update update,String parametros) throws IOException, SQLException;
    public SendMessage waitDb(Update update) throws IOException, SQLException;
    public SendMessage getListDeploy(Update update) throws IOException, SQLException;
    public SendMessage getListDeployForManaged(Update update,String managed) throws IOException, SQLException;
    public SendMessage getListProperties(Update update,String managed) throws IOException, SQLException;
    public SendMessage getListPropertiesPath(Update update,String managed) throws IOException, SQLException;
    public SendMessage doMoveProperties(Update update,String parametros,String propiedad, String path) throws IOException, SQLException;
    public SendMessage deployWeblogicPre(Update update, String parametros) throws IOException, SQLException;
    public SendMessage reDeployWeblogicQAUAT(Update update,String parametros) throws IOException, SQLException;
    public SendMessage deployProperties(Update update,String parametros) throws IOException, SQLException;

}
