package com.novopayment.infratools.helper;


import com.novopayment.infratools.core.impl.LoadProperties;
import com.novopayment.infratools.core.impl.SshBusiness;
import com.novopayment.infratools.od.Out;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.activation.DataHandler;
import javax.activation.DataSource;
import javax.activation.FileDataSource;
import javax.mail.*;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeBodyPart;
import javax.mail.internet.MimeMessage;
import javax.mail.internet.MimeMultipart;
import java.io.File;
import java.util.Date;
import java.util.List;
import java.util.Properties;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class MsnMailHelper extends Thread {
    LoadProperties loadProperties;

    public MsnMailHelper(){
        super();
    }

    private static final String PATTERN_EMAIL = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
            + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    // Datos de conexion
    private String servidorSmtp;
    private String autenticacion;
    private String puerto;
    private String usuarioSmtp;
    private String passwordSmtp;
    private String startTLS;
    private String mailDebug;
    private Properties propiedades;
    // Datos del EMAIL a enviar
    private String subject;
    private List<String> recipients;
    private String messageBody;
    private String fileName;
    private String fileURL;

    // EMAIL simple
    public MsnMailHelper(String subject, List<String> recipients, String messageBody) {
        super();
        this.cargarConfiguracion();

        this.subject = subject;
        this.recipients = recipients;
        this.messageBody = messageBody;
    }

    // EMAIL con adjunto
    public MsnMailHelper(String subject, List<String> recipients, String messageBody, String fileName, String fileURL) {
        super();
        this.cargarConfiguracion();

        this.subject = subject;
        this.recipients = recipients;
        this.messageBody = messageBody;
        this.fileName = fileName;
        this.fileURL = fileURL;
    }

    // EMAIL con adjunto
    public void LoadMsnMailHelper(Out out, LoadProperties loadProperties) {
        this.loadProperties=loadProperties;
        this.cargarConfiguracion();
        this.subject = out.getSubject();
        this.recipients = out.getRecipients();
        this.messageBody = out.getMessageBody();
        this.fileName = out.getPdf().getName();
        this.fileURL = out.getPdf().getPath();
    }

    public static boolean validarDestinatario(String dest) {
        String destinatario = dest;
        destinatario = (destinatario != null) ? destinatario : "";
        destinatario = destinatario.toLowerCase();
        Pattern p = Pattern.compile(PATTERN_EMAIL);
        Matcher m = p.matcher(destinatario);

        return m.find();
    }

    @Override
    public void run() {
        try {
            // Preparamos la Sesion autenticando al usuario
            Authenticator auth = new SMTPAuthenticator(this.getUsuarioSmtp(), this.getPasswordSmtp());
            Session session = Session.getInstance(this.getPropiedades(), auth);

            // Preparamos el Mensaje
            MimeMessage message = new MimeMessage(session);
            message.setSentDate(new Date());
            message.setFrom(new InternetAddress(loadProperties.getMail().get("mail.from")));
            message.setSubject(this.subject);

            for (String address : this.recipients) {
                if (validarDestinatario(address))
                    message.addRecipient(Message.RecipientType.TO, new InternetAddress(address));
            }

            if ((message.getAllRecipients() == null) || (message.getAllRecipients().length == 0))
                throw new Exception("No se han especificado direcciones de correo validas...");

            // Create a multipar message
            Multipart multipart = new MimeMultipart();

            // Create the message part
            MimeBodyPart messageBodyPart = new MimeBodyPart();
            messageBodyPart.setText(this.messageBody, "UTF-8", "html");
            multipart.addBodyPart(messageBodyPart);

            // Part two is attachment
            if ((this.fileName != null) && (this.fileURL != null)) {
                // Archivo Original
                messageBodyPart = new MimeBodyPart();
                DataSource source = new FileDataSource(this.fileURL);
                messageBodyPart.setDataHandler(new DataHandler(source));
                messageBodyPart.setFileName(this.fileName);
                multipart.addBodyPart(messageBodyPart);
            }

            // Send the complete message parts
            message.setContent(multipart);

            // Enviar el EMAIL
            Transport.send(message);

            //ELIMINAR ARCHIVO PDF LUEGO DE ENVIADO
            removePdf(new File(this.fileURL.substring(0,this.fileURL.lastIndexOf("/"))));

        } catch (Throwable e) {
            log.error(e.getMessage());
        }
    }

    private void cargarConfiguracion() {
        // Cargar los datos de configuracion del correo
        this.setAutenticacion(loadProperties.getMail().get("mail.smtp.auth"));
        this.setServidorSmtp((loadProperties.getMail().get("mail.host")));
        this.setPuerto((loadProperties.getMail().get("mail.smtp.port")));
        this.setStartTLS((loadProperties.getMail().get("mail.starttls")));
        this.setMailDebug((loadProperties.getMail().get("mail.debug")));
        this.setUsuarioSmtp((loadProperties.getMail().get("mail.user")));
        this.setPasswordSmtp((loadProperties.getMail().get("mail.password")));

        // Establecer las propiedades de la conexion
        this.propiedades = new Properties();
        this.propiedades.put("mail.smtp.auth", this.getAutenticacion());
        this.propiedades.put("mail.host", this.getServidorSmtp());
        this.propiedades.put("mail.smtp.port", this.getPuerto());
        this.propiedades.put("mail.smtp.starttls.enable", this.getStartTLS());
        this.propiedades.put("mail.debug", this.getMailDebug());
        this.propiedades.put("mail.smtp.user", this.getUsuarioSmtp());
    }

    // Getters y Setters
    public String getServidorSmtp() {
        return this.servidorSmtp;
    }

    public void setServidorSmtp(String servidorSmtp) {
        this.servidorSmtp = servidorSmtp;
    }

    public String getAutenticacion() {
        return this.autenticacion;
    }

    public void setAutenticacion(String autenticacion) {
        this.autenticacion = autenticacion;
    }

    public String getPuerto() {
        return this.puerto;
    }

    public void setPuerto(String puerto) {
        this.puerto = puerto;
    }

    public String getUsuarioSmtp() {
        return this.usuarioSmtp;
    }

    public void setUsuarioSmtp(String usuarioSmtp) {
        this.usuarioSmtp = usuarioSmtp;
    }

    public String getPasswordSmtp() {
        return this.passwordSmtp;
    }

    public void setPasswordSmtp(String passwordSmtp) {
        this.passwordSmtp = passwordSmtp;
    }

    public String getStartTLS() {
        return this.startTLS;
    }

    public void setStartTLS(String startTLS) {
        this.startTLS = startTLS;
    }

    public String getMailDebug() {
        return this.mailDebug;
    }

    public void setMailDebug(String mailDebug) {
        this.mailDebug = mailDebug;
    }

    public Properties getPropiedades() {
        return this.propiedades;
    }

    class SMTPAuthenticator extends Authenticator {
        String email;
        String passw;

        SMTPAuthenticator(String email, String pass) {
            super();
            this.email = email;
            this.passw = pass;
        }

        public PasswordAuthentication getPasswordAuthentication() {
            return new PasswordAuthentication(this.email, this.passw);
        }
    }


    public void removePdf(File PATH) {
        String[]entries = PATH.list();
        for(String s: entries){
            File currentFile = new File(PATH.getPath(),s);
            currentFile.delete();
        }
    }

}


