package com.novopayment.infratools.helper;

import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;

@Service
public class InfraHelper {


    public String getContentFile(String path){
        File resource;

        String content = null;
        try {
            resource = new ClassPathResource(path).getFile();
            content = new String(Files.readAllBytes(resource.toPath()));
        } catch (IOException e) {
            e.printStackTrace();
        }finally {

        }

        return content;

    }


}
