package com.novopayment.infratools.helper;

import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;
import java.util.Map;
import java.util.ResourceBundle;
import java.util.Set;
import java.util.logging.Logger;


@Slf4j
public class MailServiceHelper {

    private static final String paramFormat = "{{ name }}";
    private static final String eliminarFragmento = "{{ ELIMINAR }}";

    public MailServiceHelper() {
        super();
    }

    /* SERVICIOS */


    public Boolean validarDestinatario(String correo) {
        return MsnMailHelper.validarDestinatario(correo);
    }


    public void enviarCorreo(String subject, List<String> recipients, String pathEmailTemplate, Map<String, String> mailParams) throws Exception {
        try {
            /* Realizar la VALIDACIONES requeridas */
            // Validar el asunto
            if ((subject == null) || (subject.trim().equals(""))) {
                throw new Exception("El correo no tiene especificado un asunto!!!");
            }

            // Validar la ruta del correo
            if ((pathEmailTemplate == null) || (pathEmailTemplate.trim().equals(""))) {
                throw new Exception("No se ha especificado la ruta de la plantilla de correo!!!");
            }

            // Validar la lista de destinatarios
            if ((recipients == null) || (recipients.isEmpty())) {
                throw new Exception("La lista de destinatarios del correo esta vacia!!!");
            }

            /* Obtener y generar el contenido del correo electronico */
            String emailTemplate = this.getEmailTemplate(pathEmailTemplate);
            String messageBody = this.renderEmail(emailTemplate, mailParams);

            /* Enviar el correo */
            MsnMailHelper mensajero = new MsnMailHelper(subject, recipients, messageBody);
            mensajero.start();

        } catch (Exception e) {
            String errorMessage = "ERROR: No se ha podido enviar el correo electronico. Causa: " + e.getMessage();
            log.error(errorMessage);
            throw new Exception(errorMessage);
        }

    }

    public void enviarCorreo(String subject, List<String> recipients, String pathEmailTemplate, Map<String, String> mailParams, String fileName, String fileURL) throws Exception {
        try {
            /* Realizar la VALIDACIONES requeridas */
            // Validar el asunto
            if ((subject == null) || (subject.trim().equals(""))) {
                throw new Exception("El correo no tiene especificado un asunto!!!");
            }

            // Validar la ruta del correo
            if ((pathEmailTemplate == null) || (pathEmailTemplate.trim().equals(""))) {
                throw new Exception("No se ha especificado la ruta de la plantilla de correo!!!");
            }

            // Validar la lista de destinatarios
            if ((recipients == null) || (recipients.isEmpty())) {
                throw new Exception("La lista de destinatarios del correo esta vacia!!!");
            }

            // Validar la ruta del archivo
            if (fileURL == null) {
                throw new Exception("La URL del archivo adjunto no es valida!!!");
            }

            /* Obtener y generar el contenido del correo electronico */
            String emailTemplate = this.getEmailTemplate(pathEmailTemplate);
            String messageBody = this.renderEmail(emailTemplate, mailParams);

            /* Enviar el correo */
            MsnMailHelper mensajero = new MsnMailHelper(subject, recipients, messageBody, fileName, fileURL);
            mensajero.start();

        } catch (Exception e) {
            String errorMessage = "ERROR: No se ha podido enviar el correo electronico. Causa: " + e.getMessage();
            log.error(errorMessage);
            throw new Exception(errorMessage);
        }

    }

    public synchronized String getEmailTemplate(String pathEmailTemplate) throws Exception {
        StringBuilder template = new StringBuilder();

        try {
            // Volcamos lo recibido al buffer
            InputStream inputStream = new FileInputStream(pathEmailTemplate);
            BufferedReader in = new BufferedReader(new InputStreamReader(inputStream, "UTF-8"));

            String line;
            while ((line = in.readLine()) != null)
                template.append(line);
            in.close();
            inputStream.close();

        } catch (Exception e) {
            String errorMessage = "No se ha podido obtener la plantilla del correo electronico. Causa: " + e.getMessage();
            throw new Exception(errorMessage);
        }

        template.trimToSize();
        return template.toString();
    }

    public String renderEmail(String pEmailTemplate, Map<String, String> mailParams) throws Exception {
        String emailTemplate = pEmailTemplate;
        try {
            /* Realizar la VALIDACIONES requeridas */
            // Validar el asunto
            if ((emailTemplate == null) || (emailTemplate.trim().equals(""))) {
                throw new Exception("Email en blanco");
            }

            // Validar la lista de parametros
            if (mailParams == null) {
                throw new Exception("Correo sin parametros");
            }

            /* Sustituir el VALOR de los parametros en la plantilla */
            Set<String> params = mailParams.keySet();
            for (String paramName : params) {
                String parametro = MailServiceHelper.paramFormat.replace("name", paramName);
                emailTemplate = emailTemplate.replace(parametro, mailParams.get(paramName));
            }

            /* Eliminar bloques ocultos en el correo */
            int indexFrom = emailTemplate.indexOf(MailServiceHelper.eliminarFragmento);
            while (indexFrom != -1) {
                int indexTo = emailTemplate.indexOf(MailServiceHelper.eliminarFragmento, (indexFrom + MailServiceHelper.eliminarFragmento.length()));
                String eliminar = emailTemplate.substring(indexFrom, indexTo + MailServiceHelper.eliminarFragmento.length() + 1);
                emailTemplate = emailTemplate.replace(eliminar, "");

                indexFrom = emailTemplate.indexOf(MailServiceHelper.eliminarFragmento);
            }

        } catch (Exception e) {
            String errorMessage = "No se ha podido obtener el contenido del correo electronico. Causa: " + e.getMessage();
            throw new Exception(errorMessage);
        }

        return emailTemplate;
    }


    public String getMailsPath() {
        return ResourceBundle.getBundle("mail").getString("emailsPath");
    }
}
