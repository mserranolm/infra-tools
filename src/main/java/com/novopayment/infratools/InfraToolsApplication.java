package com.novopayment.infratools;

import com.novopayment.infratools.chatbots.IIntegratorChatBootsBusiness;
import com.novopayment.infratools.chatbots.impl.TelegramBots;
import com.novopayment.infratools.chatbots.impl.TelegramBotsDev;
import com.novopayment.infratools.chatbots.impl.TelegramBotsQa;
import com.novopayment.infratools.chatbots.impl.TelegramBotsUat;
import com.novopayment.infratools.core.impl.LoadProperties;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.properties.EnableConfigurationProperties;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.telegram.telegrambots.ApiContextInitializer;
import org.telegram.telegrambots.TelegramBotsApi;
import org.telegram.telegrambots.exceptions.TelegramApiException;

@Slf4j
@SpringBootApplication
public class InfraToolsApplication extends SpringBootServletInitializer implements ApplicationListener<ContextRefreshedEvent> {

	public static void main(String[] args) {
		SpringApplication.run(InfraToolsApplication.class, args);
	}

	@Override
	public void onApplicationEvent(ContextRefreshedEvent cre) {
		try {
			// Evidenciar en el LOG el inicio correcto de los servicios
			log.info("INICIANDO INFRATOOLS");

			ApiContextInitializer.init();
			// Instantiate Telegram Bots API

			TelegramBotsApi botsApi = new TelegramBotsApi();

			// Register our bot
			try {
				botsApi.registerBot(new TelegramBots(integratorChatBootsBusiness,loadProperties));
				botsApi.registerBot(new TelegramBotsQa(integratorChatBootsBusiness,loadProperties));
				botsApi.registerBot(new TelegramBotsDev(integratorChatBootsBusiness,loadProperties));
				botsApi.registerBot(new TelegramBotsUat(integratorChatBootsBusiness,loadProperties));
			} catch (TelegramApiException e) {
				e.printStackTrace();
			}

		} catch (Exception e) {
			log.error(e.getMessage(), e);
		}
	}


	@Autowired
	IIntegratorChatBootsBusiness integratorChatBootsBusiness;
	@Autowired
	LoadProperties loadProperties;


}
