package com.novopayment.infratools.core;

import java.io.IOException;
import java.sql.SQLException;

public interface ICallServiceBusiness {
    Object callService(Object request, String url, Object response) throws IOException, SQLException;
}
