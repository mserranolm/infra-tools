package com.novopayment.infratools.core.impl;

import com.itextpdf.text.Document;
import com.itextpdf.text.pdf.PdfWriter;
import com.itextpdf.tool.xml.XMLWorkerHelper;
import com.novopayment.infratools.helper.MsnMailHelper;
import com.novopayment.infratools.od.Constants;
import com.novopayment.infratools.od.Out;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.font.TextAttribute;
import java.awt.image.BufferedImage;
import java.io.*;
import java.text.AttributedString;
import java.util.*;
import java.util.logging.Logger;

@Slf4j
@Service
public class ReportBusiness {
    private LoadProperties loadProperties;

    @Autowired
    public ReportBusiness(LoadProperties loadProperties) {
        this.loadProperties=loadProperties;
    }


    public void ReportCore(Out out) {
        long time;
        try {
            time = System.currentTimeMillis();
            //GENERAR EVIDENCIA
            generateEvidence(out);
            //GENERAR HTML
            out =generateHTML(out);
            //GENERO EL PDF SEGUN EL HTML ARMADO
            out =generatePDF(out);
            //ENVIO DE CORREO

            MsnMailHelper msnMailHelper=new MsnMailHelper();
            msnMailHelper.LoadMsnMailHelper(out,loadProperties);
            msnMailHelper.start();

            time = System.currentTimeMillis() - time;
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            throw e;
        }finally {
        }

    }

    public Out getTemplateHtml(Out out) {
        long time;
        try {
            time = System.currentTimeMillis();
            ClassLoader classLoader = getClass().getClassLoader();
            out.setHtml(new StringBuffer(IOUtils.toString(classLoader.getResourceAsStream(Constants.PLANTILLA),"UTF-8")));
            FileUtils.writeByteArrayToFile(new File(out.getPath().getPath()+File.separator+"logo.png"), IOUtils.toByteArray(getClass().getClassLoader().getResourceAsStream("logo.png")));
            out.setHtml(new StringBuffer(out.getHtml().toString().replaceAll("@DivLOGO@", out.getPath().getPath()+File.separator+"logo.png")));
            out.setHtml(new StringBuffer(out.getHtml().toString().replaceAll("@ServidorNombre@", out.getTitle())));
            out.setHtml(new StringBuffer(out.getHtml().toString().replaceAll("@user@", out.getUser())));
            time = System.currentTimeMillis() - time;
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return out;
    }

    public Out generatePDF(Out out) {
        File file = new File(Constants.PATHPDF+ File.separator+out.getName()+File.separator+ out.getName().concat(Constants.EXTPDF));
        byte [] pdf = new byte[0];
        long time;
        try {
            time = System.currentTimeMillis();
            Document document = new Document();
            PdfWriter writer = PdfWriter.getInstance(document, new FileOutputStream(file));
            document.open();
            XMLWorkerHelper.getInstance().parseXHtml(writer, document,
                    new ByteArrayInputStream(out.getHtml().toString().getBytes()));
            document.close();

            out.setPdf(file);
            time = System.currentTimeMillis() - time;
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return out;
    }

    public Out generateHTML(Out out) {

        long time;

        log.info("Formateando HTML");

        try {
            time = System.currentTimeMillis();
            out =getTemplateHtml(out);

            Iterator it = out.getFile().entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry file = (Map.Entry)it.next();
                String imagen = null;
                String imgPath= out.getPath().getPath()+File.separator+file.getKey()+".png";

                imagen = "<div class='col-xs-12'>" +
                        "   <div class='resultado'>" +
                        "       <div style='background: #0180BD;color: white;padding: 5px;'>" +
                        "           <strong style='color:white;'>" + file.getKey() + "</strong>" +
                        "       </div><br/>" +
                        "       <img src='" + imgPath + "' />" +
                        "   </div>" +
                        "</div><br/> @DivImagen@";

                String html = out.getHtml().toString().replaceAll("@DivImagen@", imagen);
                out.setHtml(new StringBuffer(html));
            }

            out.setHtml(new StringBuffer(out.getHtml().toString().replaceAll(" @DivImagen@","")));
            time = System.currentTimeMillis() - time;
            log.info("Finalizado Formateando HTML");

        } catch (Exception e) {
            e.printStackTrace();
        }
        return out;
    }

    public void textToImage(String nombre, StringBuffer br) {
        long time;
        log.info("Creando evidencias de " + nombre);
        /*
           Because font metrics is based on a graphics context, we need to create
           a small, temporary image so we can ascertain the width and height
           of the final image
         */
        time = System.currentTimeMillis();
        BufferedImage img = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g2d = img.createGraphics();

        int height = getHeight(br);

        int width = getwidth(br);

        int fontSize = 0;

        Font  font = new Font("Monospaced", Font.PLAIN, 14);

        fontSize = 15;

        g2d.dispose();

        img = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        g2d = img.createGraphics();
        g2d.setFont(font);
        g2d.setRenderingHint(RenderingHints.KEY_ALPHA_INTERPOLATION, RenderingHints.VALUE_ALPHA_INTERPOLATION_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_COLOR_RENDERING, RenderingHints.VALUE_COLOR_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_DITHERING, RenderingHints.VALUE_DITHER_ENABLE);
        g2d.setRenderingHint(RenderingHints.KEY_FRACTIONALMETRICS, RenderingHints.VALUE_FRACTIONALMETRICS_ON);
        g2d.setRenderingHint(RenderingHints.KEY_INTERPOLATION, RenderingHints.VALUE_INTERPOLATION_BILINEAR);
        g2d.setRenderingHint(RenderingHints.KEY_RENDERING, RenderingHints.VALUE_RENDER_QUALITY);
        g2d.setRenderingHint(RenderingHints.KEY_STROKE_CONTROL, RenderingHints.VALUE_STROKE_PURE);

        g2d.setColor(Color.WHITE);
        g2d.fillRect(0, 0, width, height);
        g2d.setColor(Color.BLACK);

        int nextLinePosition = 100;

        String line;

        String[] lineas = br.toString().split("\n");
        AttributedString a = null;

        ArrayList<String> key=getKeyStringArray((HashMap<String, String>) loadProperties.getColor());

        if (key.size()==0){
            for (String linea : lineas) {
                g2d.drawString(linea, 0, nextLinePosition);
                nextLinePosition = nextLinePosition + fontSize;
            }
        }else{
            for (String linea : lineas) {
                a = new AttributedString(linea);
                boolean in = false;
                for(String aux : key){
                    if (linea.indexOf(aux)!=-1){
                        String [] rgb=getValueStatus((HashMap<String, String>) loadProperties.getColor(),aux);
                        a.addAttribute(TextAttribute.FONT, font);
                        a.addAttribute(TextAttribute.FOREGROUND, new Color(Integer.valueOf(rgb[0]),Integer.valueOf(rgb[1]),Integer.valueOf(rgb[2])), linea.indexOf(aux), linea.indexOf(aux) + aux.length());
                        in = true;
                    }
                }
                if(in){
                    g2d.drawString(a.getIterator(), 0, nextLinePosition);
                    nextLinePosition = nextLinePosition + fontSize;
                }else{
                    g2d.drawString(linea, 0, nextLinePosition);
                    nextLinePosition = nextLinePosition + fontSize;
                }
            }
        }

        time = System.currentTimeMillis() - time;
        g2d.dispose();

        try {
            ImageIO.write(getCroppedImage(img, 0.0), "png", new File(nombre));
            log.info("Evidencia creada de nombre:  " + nombre);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public int getHeight(StringBuffer br){
        int aux=0;
        String[] lineas = br.toString().split("\n");
        for (String linea : lineas) {
            aux++;
        }
        return aux*100;
    }


    public  int getwidth(StringBuffer br){
        int aux=0;
        BufferedImage i = new BufferedImage(1, 1, BufferedImage.TYPE_INT_ARGB);
        Graphics2D g = i.createGraphics();
        String[] lineas = br.toString().split("\n");
        for (String linea : lineas) {
            if(aux<g.getFontMetrics().stringWidth(linea))
                aux=g.getFontMetrics().stringWidth(linea);
        }

        return aux*2;
    }

    public  BufferedImage getCroppedImage(BufferedImage source, double tolerance) {
        // Get our top-left pixel color as our "baseline" for cropping
        int baseColor = source.getRGB(0, 0);

        int width = source.getWidth();
        int height = source.getHeight();

        int topY = Integer.MAX_VALUE, topX = Integer.MAX_VALUE;
        int bottomY = -1, bottomX = -1;
        for (int y = 0; y < height; y++) {
            for (int x = 0; x < width; x++) {
                if (colorWithinTolerance(baseColor, source.getRGB(x, y), tolerance)) {
                    if (x < topX)
                        topX = x;
                    if (y < topY)
                        topY = y;
                    if (x > bottomX)
                        bottomX = x;
                    if (y > bottomY)
                        bottomY = y;
                }
            }
        }

        BufferedImage destination = new BufferedImage((bottomX - topX + 1),
                (bottomY - topY + 1), BufferedImage.TYPE_INT_ARGB);

        destination.getGraphics().drawImage(source, 0, 0,
                destination.getWidth(), destination.getHeight(),
                topX, topY, bottomX, bottomY, null);

        return destination;
    }

    private boolean colorWithinTolerance(int a, int b, double tolerance) {
        int aAlpha = (a & 0xFF000000) >>> 24;   // Alpha level
        int aRed = (a & 0x00FF0000) >>> 16;   // Red level
        int aGreen = (a & 0x0000FF00) >>> 8;    // Green level
        int aBlue = a & 0x000000FF;            // Blue level

        int bAlpha = (b & 0xFF000000) >>> 24;   // Alpha level
        int bRed = (b & 0x00FF0000) >>> 16;   // Red level
        int bGreen = (b & 0x0000FF00) >>> 8;    // Green level
        int bBlue = b & 0x000000FF;            // Blue level

        double distance = Math.sqrt((aAlpha - bAlpha) * (aAlpha - bAlpha) +
                (aRed - bRed) * (aRed - bRed) +
                (aGreen - bGreen) * (aGreen - bGreen) +
                (aBlue - bBlue) * (aBlue - bBlue));

        // 510.0 is the maximum distance between two colors
        // (0,0,0,0 -> 255,255,255,255)
        double percentAway = distance / 510.0d;

        return (percentAway > tolerance);
    }

    public  ArrayList<String> getKeyStringArray(HashMap<String, String> properties) {
        ArrayList<String> result = new ArrayList<String>();

        for(String key: properties.keySet()){
            result.add(key);
        }
        return result;
    }



    public String[] getValueStatus(HashMap<String, String> properties,String key) {
        String[] result;
        result=properties.get(key).split(",");

        return result;
    }

    public void generateEvidence(Out out){

        new File(Constants.PATHPDF+File.separator+ out.getName()+File.separator).mkdirs();
        out.setPath(new File(Constants.PATHPDF+File.separator+ out.getName()+File.separator));

        HashMap<String,byte[]> imgs=new HashMap<>();

        Iterator it = out.getFile().entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry file = (Map.Entry)it.next();
            try {
                textToImage(out.getPath().getPath()+File.separator+file.getKey()+".png", new StringBuffer(new String((byte[])file.getValue(),"UTF-8")));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }

}
