package com.novopayment.infratools.core.impl;



import lombok.extern.slf4j.Slf4j;
import oracle.jdbc.pool.OracleDataSource;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;


/**
 * Created by mserrano on 18/01/17.
 */

@Slf4j
@Repository
public class ConnectionOracle {
    private Connection connection;

    /**
     * La fuente de datos que apunta al repositorio de base de datos
     */
    private OracleDataSource dataSource;

    /**
     * El constructor de la clase
     */
    public ConnectionOracle(Connection connection) {
        this.connection=connection;
    }

    /**
     * El constructor de la clase
     */
    public ConnectionOracle() {
    }

    /**
     * Obtiene la fuente de datos
     *
     * @return La fuente de datos que apunta al repositorio de base de datos
     */
    public DataSource getDataSource() {
        return dataSource;
    }

    /**
     * Asigna la fuente de datos
     *
     * @param dataSource La fuente de datos que apunta al repositorio de base de datos
     */
    public void setDataSource(OracleDataSource dataSource) {
        this.dataSource = dataSource;
    }

    /**
     * Obtiene una conexi�n a la base de datos
     *
     * @return La conexi�n a la base de datos
     * @throws SQLException
     */
    public void connect(String host, String port, String name, String servicename,String username, String password) throws Exception {

        try {
            dataSource = new OracleDataSource();
            if ((name!=null)&&(!name.isEmpty())) {
                dataSource.setURL("jdbc:oracle:thin:@" + host + ":" + port + "/" + name);
            }else{
                dataSource.setURL("jdbc:oracle:thin:@" + host + ":" + port + ":" + servicename);
            }
            dataSource.setUser(username);
            dataSource.setPassword(password);
            log.info("----Iniciando conexion: " + name + "----");
            this.connection = dataSource.getConnection();
            log.info("Conexion exitosa: " + name + "----");
        } catch (SQLException e) {
            log.error("Error conectando a base de datos "+e.getMessage());
        }
    }

    /**
     * Metodo que cierra los objetos de base de datos
     *
     * @param statement  El objeto statement
     * @param resultado
     */
    public void closeJdbcObjects(Statement statement, int resultado) throws Exception {
        closeJdbcObjects(statement, null);
    }

    /**
     * Metodo que cierra los objetos de base de datos
     *
     * @param statement  El objeto statement
     * @param result     El objeto result
     */
    public void closeJdbcObjects(Statement statement, ResultSet result) throws Exception {
        try {
            if (result != null) result.close();
        } catch (SQLException e) {

        }

        try {
            if (statement != null) statement.close();
        } catch (SQLException e) {

        }

        try {
            if (this.connection != null) this.connection.close();
        } catch (SQLException e) {

        }
    }

    public Connection getConnection() {
        return connection;
    }

    public void setConnection(Connection connection) {
        this.connection = connection;
    }
}
