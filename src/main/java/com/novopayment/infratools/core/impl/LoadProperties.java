package com.novopayment.infratools.core.impl;


import lombok.extern.slf4j.Slf4j;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.stereotype.Service;


import java.util.ArrayList;
import java.util.List;
import java.util.Map;

@Slf4j
@Service
@ConfigurationProperties
public class LoadProperties {


    private Map<String, String> prop;
    private Map<String, String> credential;
    private Map<String, String> console_credential;
    private Map<String, String> deploy;
    private Map<String, String> general;
    private Map<String, String> color;
    private Map<String, String> mail;
    private Map<String, String> ambientes;
    private Map<String, String> user_auth;
    private Map<String, String> bd;
    private Map<String, String> notified_query_db;
    private Map<String, String> healthcheck;
    private Map<String, String> bot_auth;


    public Map<String, String> getCredential() {
        return credential;
    }

    public void setCredential(Map<String, String> credential) {
        this.credential = credential;
    }

    public Map<String, String> getProp() {
        return prop;
    }

    public void setProp(Map<String, String> prop) {
        this.prop = prop;
    }

    public Map<String, String> getConsole_credential() {
        return console_credential;
    }

    public void setConsole_credential(Map<String, String> console_credential) {
        this.console_credential = console_credential;
    }

    public Map<String, String> getDeploy() {
        return deploy;
    }

    public void setDeploy(Map<String, String> deploy) {
        this.deploy = deploy;
    }

    public Map<String, String> getGeneral() {
        return general;
    }

    public Map<String, String> getAmbientes() {
        return ambientes;
    }

    public Map<String, String> getUser_auth() {
        return user_auth;
    }

    public Map<String, String> getBd() {
        return bd;
    }

    public void setBd(Map<String, String> bd) {
        this.bd = bd;
    }

    public void setUser_auth(Map<String, String> user_auth) {
        this.user_auth = user_auth;
    }

    public void setAmbientes(Map<String, String> ambientes) {
        this.ambientes = ambientes;
    }

    @Override
    public String toString() {
        return "LoadProperties{" +
                "prop=" + prop +
                ", credential=" + credential +
                ", console_credential=" + console_credential +
                ", deploy=" + deploy +
                ", general=" + general +
                '}';
    }

    public void setGeneral(Map<String, String> general) {
        this.general = general;
    }

    public String getListCommand() {

        String response = "";
        try {

            for (Map.Entry<String, String> entry : getProp().entrySet()) {
                response = entry.getKey() + " " + entry.getValue().split("\\|")[entry.getValue().split("\\|").length - 1];
            }

        } catch (Exception e) {

        }
        return response;
    }

    public String getCommand(String key) {

        String response = ((String) getProp().get(key)).split("\\|")[((String) getProp().get(key)).split("\\|").length - 1];

        return response;
    }

    public String getEntry(String key) {

        String response = getProp().get(key);

        return response;
    }


    public String getCredentials(String key) {
        String response="";

        for (String value : getCredential().get(key).split("\\|"))
            response = response.concat(value).concat("\n");

        return response;
    }

    public String getListCredentials() {

        String response = "";
        try {

            for (Map.Entry<String, String> entry : getCredential().entrySet()) {
                response = response + entry.getKey() + System.lineSeparator();
            }

        } catch (Exception e) {

        }
        return response;
    }

    public String getConsoleCredentials(String key) {
        String response="";

        for (String value : getConsole_credential().get(key).split("\\|"))
            response = response.concat(value).concat("\n");

        return response;
    }

    public String getListConsoleCredentials() {

        String response = "";
        try {

            for (Map.Entry<String, String> entry : getConsole_credential().entrySet()) {
                response = response + entry.getKey() + System.lineSeparator();
            }

        } catch (Exception e) {

        }
        return response;
    }


    public String getDeploy(String key) {

        String response = getDeploy().get(key);

        return response;
    }

    public String getGeneral(String key) {

        String response = getGeneral().get(key);

        return response;
    }

    public Map<String, String> getColor() {
        return color;
    }

    public void setColor(Map<String, String> color) {
        this.color = color;
    }

    public Map<String, String> getMail() {
        return mail;
    }

    public void setMail(Map<String, String> mail) {
        this.mail = mail;
    }

    public String getAmbientes(String key) {

        String response = getAmbientes().get(key);

        return response;
    }

    public String getUser_auth(String key) {

        String response = getUser_auth().get(key);

        return response;
    }

    public String getListUser_auth() {

        String response = "";
        try {

            for (Map.Entry<String, String> entry : getUser_auth().entrySet()) {
                response = response + entry.getKey() + System.lineSeparator();
            }

        } catch (Exception e) {

        }
        return response;
    }

    public String getBd(String key) {

        String response = getBd().get(key);

        return response;
    }

    public Map<String, String> getNotified_query_db() {
        return notified_query_db;
    }

    public void setNotified_query_db(Map<String, String> notified_query_db) {
        this.notified_query_db = notified_query_db;
    }

    public String getNotified_query_db(String key) {
        String response = "";

        response = getNotified_query_db().get(key);
        
        return response;
    }


    public Map<String, String> getHealthcheck() {
        return healthcheck;
    }
    public String[]  getHealthcheck(String key) {
        String[] hostList;

        hostList=getHealthcheck().get(key).split(",");

        return hostList;
    }

    public void setHealthcheck(Map<String, String> healthcheck) {
        this.healthcheck = healthcheck;
    }

    public Map<String, String> getBot_auth() {
        return bot_auth;
    }

    public String getBot_auth(String key) {
        String response = getBot_auth().get(key);
        return response;
    }

    public void setBot_auth(Map<String, String> bot_auth) {
        this.bot_auth = bot_auth;
    }
}
