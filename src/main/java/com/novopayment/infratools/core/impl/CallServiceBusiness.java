package com.novopayment.infratools.core.impl;

import com.novopayment.infratools.core.ICallServiceBusiness;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.*;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Logger;

@Slf4j
@Service
public class CallServiceBusiness implements ICallServiceBusiness {

    @Autowired
    public CallServiceBusiness() {
    }

    public Object callService(Object request, String url, Object response) throws IOException, SQLException {
        final String origen = "CallServiceBusiness.callService";
        long time;

        ResponseEntity<Class> result = null;
        try {
            log.info("Iniciando | " + origen);
            time = System.currentTimeMillis();

            Class cls = response.getClass();
            List<MediaType> acceptableMediaTypes = new ArrayList<MediaType>();
            acceptableMediaTypes.add(MediaType.APPLICATION_JSON);
            HttpHeaders headers = new HttpHeaders();
            headers.setAccept(acceptableMediaTypes);
            HttpEntity entity = new HttpEntity(request, headers);
            RestTemplate restTemplate = new RestTemplate();
            result = restTemplate.exchange(url, HttpMethod.POST, entity, cls);


            time = System.currentTimeMillis() - time;
            log.info("Iniciando | " + origen+ " " + time);
        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
        }

        return result.getBody();
    }
}
