package com.novopayment.infratools.core.impl;


import com.jcraft.jsch.Channel;
import com.jcraft.jsch.JSch;
import com.jcraft.jsch.JSchException;
import com.jcraft.jsch.Session;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Repository;

import java.util.logging.Logger;


/**
 * Created by mserrano on 24/06/16.
 */

@Slf4j
@Repository
public class ConnectionSSH {

    private Session session;


    public void openConnectSSH(String host, String port,String user, String password) {

        try {
            JSch jsch = new JSch();

            session = null;
            session = jsch.getSession(user, host, Integer.valueOf(port));
            java.util.Properties config = new java.util.Properties();
            config.put("StrictHostKeyChecking", "no");
            session.setConfig(config);
            session.setPassword(password);
            session.connect();

        } catch (JSchException e) {
            log.error("Error conectando SSH: " + e.getMessage());
        } finally {

        }

    }


    public void closetConnectSSH(Channel channel) {

        try {

            channel.disconnect();
            session.disconnect();

            log.info("Cierre de Conexion al nodo exitoso");

        } catch (Exception e) {
            log.error("Mensaje: " + e.getMessage());
        } finally {

        }

    }


    public Session getSession() {
        return session;
    }

    public void setSession(Session session) {
        this.session = session;
    }

}
