package com.novopayment.infratools.core.impl;


import com.novopayment.infratools.od.Checkup;
import com.novopayment.infratools.od.Out;
import com.novopayment.infratools.od.Response;
import com.novopayment.infratools.services.database.impl.OracleService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;


import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Logger;

@Slf4j
@Service
public class IntegratorBusiness {
    private SshBusiness sshBusiness;
    private ReportBusiness reportBusiness;
    private LoadProperties loadProperties;

    @Autowired
    public IntegratorBusiness(SshBusiness sshBusiness,ReportBusiness reportBusiness,LoadProperties loadProperties) {
        this.sshBusiness = sshBusiness;
        this.reportBusiness = reportBusiness;
        this.loadProperties = loadProperties;

    }


    public Response integratorCore(List<Checkup> checkups) throws IOException, SQLException {
        Out out =new Out();
        boolean have =false;
        final String origen = "IntegratorBusiness.integratorcore";
        long time;
        Response response=new Response();


        try {
            log.info("Iniciando "+origen);
            time = System.currentTimeMillis();
            out.setFile(new HashMap<>());
            out.setRecipients(new ArrayList<>());
            for (Checkup checkup : checkups) {
                switch (checkup.getComponent()) {
                    case "ORACLE_INSTANCE":
                        out.getFile().put(checkup.getName(), checkup.getUsername().getBytes());
                        out.setName(checkup.getName());
                        out.setRecipients(checkup.getRecipients());
                        out.setMessageBody(loadProperties.getGeneral("message_body"));
                        out.setMessageBody(out.getMessageBody().concat(" ").concat(checkup.getName()));
                        have =true;
                        break;
                    case "MYSQL_SERVER_INSTANCE":
                        //out.getFile().putAll(mysqlBusiness.execute(checkup).getFile());
                        have =true;
                        break;
                    case "LINUX_INTANCE":
                        out=sshBusiness.execute(checkup);
                        have =true;
                        break;
                    case "WEBLOGIC_DOMAIN":
                        out=sshBusiness.execute(checkup);
                        have = true;
                        break;
                }

                response.setResponse(out.getMessageBody());

                if(out.getMessageBody()!=null){
                    out.setName(checkup.getName());
                    out.setRecipients(checkup.getRecipients());
                    out.setTitle(out.getTitle()+checkup.getTitle()+" ");
                    out.setMessageBody(loadProperties.getGeneral("message_body"));
                    out.setMessageBody(out.getMessageBody().concat(" ").concat(checkup.getName()));
                    out.setUser(checkup.getUserTelegram());
                }
            }

            out.setMessageBody(out.getMessageBody());


            if(have)
                reportBusiness.ReportCore(out);

            response.setCode("0");


            time = System.currentTimeMillis() - time;

            log.info("Finalizando "+origen+ " "+time);

        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            throw e;
        }

        return response;
    }





}
