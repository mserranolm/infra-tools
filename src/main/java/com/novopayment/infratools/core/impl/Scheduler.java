package com.novopayment.infratools.core.impl;

import com.novopayment.infratools.services.database.IOracleService;
import com.novopayment.infratools.services.database.impl.OracleService;
import com.novopayment.infratools.services.monitor.IMonitorServices;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import java.io.IOException;
import java.sql.SQLException;
import java.util.logging.Logger;

@Slf4j
@Component
public class Scheduler {
    private IMonitorServices monitorServices;

    @Autowired
    public Scheduler(IMonitorServices monitorServices) {
        this.monitorServices = monitorServices;
    }

    @Scheduled(cron = "1 * * * * ?")
    public void cronJobSch() {

        log.info("Inicio de Job");

        try {
            monitorServices.liveUrl();
        } catch (IOException e) {
            e.printStackTrace();
        }

        log.info("Fin del job");
    }
}
