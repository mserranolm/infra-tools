package com.novopayment.infratools.core.impl;

import com.jcraft.jsch.*;
import com.novopayment.infratools.od.Checkup;
import com.novopayment.infratools.od.Out;
import com.novopayment.infratools.od.Script;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;
import java.io.*;
import java.util.Base64;
import java.util.HashMap;
import java.util.logging.Logger;
import java.util.stream.Collectors;

@Slf4j
@Service
public class SshBusiness {
    private ConnectionSSH connectionSSH;
    Channel channel;


    public SshBusiness(ConnectionSSH connectionSSH) {
        this.connectionSSH = connectionSSH;
    }

    public Out execute(Checkup checkup) {
        Out out = new Out();
        long time;
        try {
            time = System.currentTimeMillis();
            //Conectarme al servidor
            connectionSSH = new ConnectionSSH();
            connectionSSH.openConnectSSH(checkup.getHost(),checkup.getPort(),checkup.getUsername(),checkup.getPassword());

            out.setFile(new HashMap<>());
            for (Script script : checkup.getScript()) {
                if (script.getType().equalsIgnoreCase("BATCH_SCRIPT")) {
                    if(!script.isCommand())
                        out.getFile().put(script.getNameExecution(),String.valueOf(RunCommand(script)).getBytes());
                    else {
                        String outString = String.valueOf(RunCommand(script));
                        if(!outString.equalsIgnoreCase("")) {
                            out.getFile().put(script.getNameExecution(), outString.getBytes());
                            out.setMessageBody(outString);
                        }
                    }

                }
            }
            time = System.currentTimeMillis() - time;

        } catch (Exception e) {
            log.error("Failed to Execute, The error is" + e.getMessage());
            throw e;
        }finally {
            connectionSSH.closetConnectSSH(channel);
            channel.disconnect();
        }

        return out;
    }


    public StringBuffer RunCommand(Script script) {
        StringBuffer out = null;
        BufferedReader stdInput = null;
        BufferedReader stderror = null;

        long time;
        try {
            time = System.currentTimeMillis();

            String command;
            if(!script.isCommand())
                command= new String(Base64.getDecoder().decode(script.getFile()));
            else
                command=script.getFile();

            channel = connectionSSH.getSession().openChannel("exec");
            ((ChannelExec) channel).setCommand(command);
            channel.setInputStream(null);
            InputStream stderr = ((ChannelExec)channel).getErrStream();
            InputStream in = channel.getInputStream();
            channel.connect();

            stdInput = new BufferedReader(new InputStreamReader(in));
            stderror = new BufferedReader(new InputStreamReader(stderr));

            int count = 0;

            out = new StringBuffer(stderror.lines().collect(Collectors.joining("\n")));

            if (out.toString().equalsIgnoreCase(""))
                out = new StringBuffer(stdInput.lines().collect(Collectors.joining("\n")));


            time = System.currentTimeMillis() - time;

        } catch (JSchException e) {
            log.error("Mensaje: Error de conexion " + e.getMessage());
        } catch (IOException e) {
            log.error("Mensaje: Error de conexion " + e.getMessage());
        } finally {

        }

        return out;

    }


    public void sendFileSCP(Checkup checkup, String Path) throws SftpException {
        
        long time;
        try {
            time = System.currentTimeMillis();

            //Conectarme al servidor
            connectionSSH = new ConnectionSSH();
            connectionSSH.openConnectSSH(checkup.getHost(),checkup.getPort(),checkup.getUsername(),checkup.getPassword());

            String command= new String(Base64.getDecoder().decode(checkup.getScript().get(0).getFile()));

            channel = connectionSSH.getSession().openChannel("sftp");
            channel.connect();

            ByteArrayInputStream bis = new ByteArrayInputStream(command.getBytes());

            ((ChannelSftp) channel).cd(Path);
            ((ChannelSftp) channel).put(bis, checkup.getScript().get(0).getNameExecution());

            time = System.currentTimeMillis() - time;


        } catch (JSchException e) {
            log.error("Mensaje: " + e.getMessage());
        } finally {

        }

    }

    public void sendFileSCPByte(Checkup checkup, String Path) throws SftpException {

        long time;
        try {
            time = System.currentTimeMillis();

            //Conectarme al servidor
            connectionSSH = new ConnectionSSH();
            connectionSSH.openConnectSSH(checkup.getHost(),checkup.getPort(),checkup.getUsername(),checkup.getPassword());

            channel = connectionSSH.getSession().openChannel("sftp");
            channel.connect();

            ByteArrayInputStream bis = new ByteArrayInputStream(Base64.getDecoder().decode(checkup.getScript().get(0).getFile().getBytes()));

            File f = new File(Path);
            ((ChannelSftp) channel).cd(Path.replace(f.getName(),""));
            ((ChannelSftp) channel).put(bis, f.getName());

            time = System.currentTimeMillis() - time;


        } catch (JSchException e) {
                log.error("Mensaje: " + e.getMessage());
        } finally {

        }

    }

    public ByteArrayOutputStream receiveFileSCPByte(Checkup checkup, String Path) throws SftpException {
        ByteArrayOutputStream bos = null;
        long time;
        try {
            time = System.currentTimeMillis();

            //Conectarme al servidor
            connectionSSH = new ConnectionSSH();
            connectionSSH.openConnectSSH(checkup.getHost(),checkup.getPort(),checkup.getUsername(),checkup.getPassword());

            channel = connectionSSH.getSession().openChannel("sftp");
            channel.connect();

            bos = new ByteArrayOutputStream();

            ((ChannelSftp) channel).get(Path, bos);

            time = System.currentTimeMillis() - time;


        } catch (JSchException e) {
            log.error("Mensaje: " + e.getMessage());
        } finally {

        }

    return bos;
    }

    public StringBuffer RunCommand(Script script) {
        StringBuffer out = null;
        BufferedReader stdInput = null;
        BufferedReader stderror = null;

        long time;
        try {
            time = System.currentTimeMillis();

            String command;
            if(!script.isCommand())
                command= new String(Base64.getDecoder().decode(script.getFile()));
            else
                command=script.getFile();

            channel = connectionSSH.getSession().openChannel("exec");
            ((ChannelExec) channel).setCommand(command);
            channel.setInputStream(null);
            InputStream stderr = ((ChannelExec)channel).getErrStream();
            InputStream in = channel.getInputStream();
            channel.connect();

            stdInput = new BufferedReader(new InputStreamReader(in));
            stderror = new BufferedReader(new InputStreamReader(stderr));

            int count = 0;

            out = new StringBuffer(stderror.lines().collect(Collectors.joining("\n")));

            if (out.toString().equalsIgnoreCase(""))
                out = new StringBuffer(stdInput.lines().collect(Collectors.joining("\n")));


            time = System.currentTimeMillis() - time;

        } catch (JSchException e) {
            log.error("Mensaje: Error de conexion " + e.getMessage());
        } catch (IOException e) {
            log.error("Mensaje: Error de conexion " + e.getMessage());
        } finally {

        }

        return out;

    }


    public void sendFileSCP(Checkup checkup, String Path) throws SftpException {

        long time;
        try {
            time = System.currentTimeMillis();

            //Conectarme al servidor
            connectionSSH = new ConnectionSSH();
            connectionSSH.openConnectSSH(checkup.getHost(),checkup.getPort(),checkup.getUsername(),checkup.getPassword());

            String command= new String(Base64.getDecoder().decode(checkup.getScript().get(0).getFile()));

            channel = connectionSSH.getSession().openChannel("sftp");
            channel.connect();

            ByteArrayInputStream bis = new ByteArrayInputStream(command.getBytes());

            ((ChannelSftp) channel).cd(Path);
            ((ChannelSftp) channel).put(bis, checkup.getScript().get(0).getNameExecution());

            time = System.currentTimeMillis() - time;


        } catch (JSchException e) {
            log.error("Mensaje: " + e.getMessage());
        } finally {

        }

    }

    public void sendFileSCPByte(Checkup checkup, String Path) throws SftpException {

        long time;
        try {
            time = System.currentTimeMillis();

            //Conectarme al servidor
            connectionSSH = new ConnectionSSH();
            connectionSSH.openConnectSSH(checkup.getHost(),checkup.getPort(),checkup.getUsername(),checkup.getPassword());

            channel = connectionSSH.getSession().openChannel("sftp");
            channel.connect();

            ByteArrayInputStream bis = new ByteArrayInputStream(Base64.getDecoder().decode(checkup.getScript().get(0).getFile().getBytes()));

            File f = new File(Path);
            ((ChannelSftp) channel).cd(Path.replace(f.getName(),""));
            ((ChannelSftp) channel).put(bis, f.getName());

            time = System.currentTimeMillis() - time;


        } catch (JSchException e) {
            log.error("Mensaje: " + e.getMessage());
        } finally {

        }

    }

    public ByteArrayOutputStream receiveFileSCPByte(Checkup checkup, String Path) throws SftpException {
        ByteArrayOutputStream bos = null;
        long time;
        try {
            time = System.currentTimeMillis();

            //Conectarme al servidor
            connectionSSH = new ConnectionSSH();
            connectionSSH.openConnectSSH(checkup.getHost(),checkup.getPort(),checkup.getUsername(),checkup.getPassword());

            channel = connectionSSH.getSession().openChannel("sftp");
            channel.connect();

            bos = new ByteArrayOutputStream();

            ((ChannelSftp) channel).get(Path, bos);

            time = System.currentTimeMillis() - time;


        } catch (JSchException e) {
            log.error("Mensaje: " + e.getMessage());
        } finally {

        }

        return bos;
    }

    public void receiveFileSCP(Checkup checkup, String Path) throws SftpException {
        long time;
        try {
            time = System.currentTimeMillis();

            //Conectarme al servidor
            connectionSSH = new ConnectionSSH();
            connectionSSH.openConnectSSH(checkup.getHost(),checkup.getPort(),checkup.getUsername(),checkup.getPassword());

            channel = connectionSSH.getSession().openChannel("sftp");
            channel.connect();

            ByteArrayOutputStream bos = new ByteArrayOutputStream();

            ((ChannelSftp) channel).get(Path, bos);

            FileOutputStream fos = new FileOutputStream("/tmp/".concat(Path.split("\\/")[Path.split("\\/").length-1]));
            bos.writeTo(fos);

            time = System.currentTimeMillis() - time;


        } catch (JSchException | IOException e) {
            log.error("Mensaje: " + e.getMessage());
        } finally {

        }
    }

}
